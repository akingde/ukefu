CREATE TABLE `uk_pbxhost_orgi_rela` (

    `id` varchar(32) NOT NULL COMMENT '主键ID',
    `pbxhostid` varchar(32) DEFAULT NULL COMMENT '联系人id',
    `orgi` varchar(200) DEFAULT NULL COMMENT '关联的租户id',
    `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
    `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
    `createtime` datetime DEFAULT NULL COMMENT '创建时间',
    `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='语音平台 和 租户 绑定';

ALTER TABLE uk_user ADD tenant tinyint(4) DEFAULT 0 COMMENT '是否是新版多租户中新增的用户（true 是）';

ALTER TABLE uk_tenant ADD agentnum INT(11) DEFAULT 0 COMMENT '文本坐席数量 （云平台坐席限制使用）';
ALTER TABLE uk_tenant ADD callcenteragentnum INT(11) DEFAULT 0 COMMENT '呼叫中心坐席数量 （云平台坐席限制使用）';
ALTER TABLE uk_tenant ADD models VARCHAR(500) DEFAULT NULL COMMENT '授权模块';

ALTER TABLE uk_tenant ADD datastatus tinyint(4) DEFAULT '0' COMMENT '数据状态（0 未删除| 1 已删除到回收站）';

ALTER TABLE uk_callcenter_pbxhost ADD defaultpbx tinyint(4) DEFAULT '0' COMMENT '是否默认 分配的语音服务器';


ALTER TABLE uk_tenant ADD robotagentnum INT(11) DEFAULT 0 COMMENT '电销机器人数量 （云平台坐席限制使用）';

ALTER TABLE uk_systemconfig ADD robotagentnum INT(11) DEFAULT 0 COMMENT '电销机器人数量 （云平台坐席限制使用）';
ALTER TABLE uk_systemconfig ADD enablecloud tinyint(4) DEFAULT 0 COMMENT '是否云平台服务';


ALTER TABLE uk_blacklist ADD listtype varchar(50) DEFAULT NULL COMMENT '名单类型(blacklist、黑名单  redlist、红名单  viplist、vip名单)';
ALTER TABLE uk_blacklist ADD extentions varchar(2000) DEFAULT NULL COMMENT '分机号';
ALTER TABLE uk_blacklist ADD skillname varchar(32) DEFAULT NULL COMMENT '技能组名称';

ALTER TABLE uk_callcenter_extention ADD assigned tinyint(4) DEFAULT 0 COMMENT '是否分配 多租户使用';

ALTER TABLE uk_sysdic ADD tenant tinyint(4) DEFAULT 0 COMMENT '是否区分多租户';

-- ----------------------------
-- 更新 系统模板分类 中 输入参数 和 输出参数 （智能机器人接口中使用，区分租户）
-- ----------------------------
update uk_sysdic set tenant = 1 where id in ('4028811b642af06f01642af9cfa304c6','4028811b642af06f01642af9cfaf04c7');


ALTER TABLE uk_reportmodel ADD cubecontent longtext DEFAULT NULL COMMENT '发布模型数据';

ALTER TABLE uk_tabletask ADD share tinyint(4) DEFAULT 0 COMMENT '是否共享（多租户）';

ALTER TABLE uk_callcenter_event ADD conference tinyint DEFAULT 0 COMMENT '是否会议通话';
ALTER TABLE uk_callcenter_event ADD conferenceinitiator tinyint DEFAULT 0 COMMENT '是否会议发起方';
ALTER TABLE uk_callcenter_event ADD inconferenecetime datetime DEFAULT NULL COMMENT '进入会议时间';
ALTER TABLE uk_callcenter_event ADD conferenceduration int DEFAULT 0 COMMENT '会议时长';
ALTER TABLE uk_callcenter_event ADD conferencenum varchar(50) DEFAULT 0 COMMENT '会议号码';
ALTER TABLE uk_callcenter_event ADD conferenceid varchar(50) DEFAULT NULL COMMENT '会议ID';


ALTER TABLE uk_xiaoe_topic ADD enablecc tinyint DEFAULT 0 COMMENT '启用呼叫中心功能';
ALTER TABLE uk_xiaoe_topic ADD cctype varchar(50) DEFAULT NULL COMMENT '呼叫中心类型';

ALTER TABLE uk_xiaoe_topic ADD cctransnum varchar(50) DEFAULT NULL COMMENT '呼叫中心转接号码';

ALTER TABLE uk_xiaoe_topic ADD ccdelay int(11) DEFAULT 0 COMMENT '呼叫中心挂断延迟';

ALTER TABLE `uk_callcenter_router`
    MODIFY COLUMN `field` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '字段名称';
ALTER TABLE `uk_callcenter_routeritem`
    MODIFY COLUMN `field` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '匹配参数名称';


ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `level` varchar(50) DEFAULT NULL COMMENT '话术评价等级';
ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `levelscore` int(11) DEFAULT 0 COMMENT '话术评价分数';
ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `focustimes` int(11) DEFAULT 0 COMMENT '话术关注点次数';
ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `processid` varchar(50) DEFAULT NULL COMMENT '话术or问卷id';
ALTER TABLE `uk_que_result_answer`
    ADD COLUMN `resultquesid` varchar(50) DEFAULT NULL COMMENT '话术问卷节点结果id';
ALTER TABLE `uk_que_result_point`
    ADD COLUMN `resultquesid` varchar(50) DEFAULT NULL COMMENT '话术问卷节点结果id';

ALTER TABLE `uk_que_result_point`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';


ALTER TABLE `uk_que_result_question`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';

ALTER TABLE `uk_que_result`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';
ALTER TABLE `uk_que_result_answer`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';


ALTER TABLE uk_systemconfig ADD ttsservice varchar(50) DEFAULT NULL COMMENT 'tts语音服务平台';
ALTER TABLE uk_systemconfig ADD xunfeittsappid varchar(255) DEFAULT NULL COMMENT 'tts-appid';
ALTER TABLE uk_systemconfig ADD xunfeittsapikey varchar(255) DEFAULT NULL COMMENT 'tts-apikey';

ALTER TABLE uk_callcenter_skillext ADD calltimeout int(11) DEFAULT 60 COMMENT '呼叫等待超时时长';

ALTER TABLE uk_tenant ADD province varchar(100) DEFAULT NULL COMMENT '省份';
ALTER TABLE uk_tenant ADD city varchar(100) DEFAULT NULL COMMENT '城市';
ALTER TABLE uk_tenant ADD area varchar(100) DEFAULT NULL COMMENT '区 县';
ALTER TABLE uk_tenant ADD addr varchar(500) DEFAULT NULL COMMENT '地址';
ALTER TABLE uk_tenant ADD mobile varchar(100) DEFAULT NULL COMMENT '联系电话';
ALTER TABLE uk_tenant ADD mail varchar(100) DEFAULT NULL COMMENT '邮箱';
ALTER TABLE uk_tenant ADD qq varchar(100) DEFAULT NULL COMMENT 'qq';
ALTER TABLE uk_tenant ADD wechat varchar(100) DEFAULT NULL COMMENT '微信号码';
ALTER TABLE uk_tenant ADD aliwangwang varchar(100) DEFAULT NULL COMMENT '阿里旺旺号码';
ALTER TABLE uk_tenant ADD description varchar(500) DEFAULT NULL COMMENT '店铺描述';
ALTER TABLE uk_tenant ADD tag varchar(100) DEFAULT NULL COMMENT '商户标签';
ALTER TABLE uk_tenant ADD servcieperiod datetime DEFAULT NULL COMMENT '服务有效期';
ALTER TABLE uk_tenant ADD type varchar(100) DEFAULT NULL COMMENT '店铺类型';
ALTER TABLE uk_tenant ADD category varchar(100) DEFAULT NULL COMMENT '店铺分类';
ALTER TABLE uk_tenant ADD grouping varchar(100) DEFAULT NULL COMMENT '店铺分组';
ALTER TABLE uk_tenant ADD scorestar DECIMAL(11,2) DEFAULT 0 COMMENT '评价星级';
ALTER TABLE uk_tenant ADD level DECIMAL(11,2) DEFAULT 0 COMMENT '店铺等级';
ALTER TABLE uk_tenant ADD mainbusiness varchar(500) DEFAULT NULL COMMENT '主营业务';
ALTER TABLE uk_tenant ADD pcurl varchar(500) DEFAULT NULL COMMENT 'pc店铺地址';
ALTER TABLE uk_tenant ADD h5url varchar(500) DEFAULT NULL COMMENT '移动端店铺地址';

ALTER TABLE uk_consult_invite ADD timeout int(11) DEFAULT 3 COMMENT '在线访客超时时长';


ALTER TABLE uk_agentservice ADD mobile varchar(10) DEFAULT NULL COMMENT '是否移动端';
ALTER TABLE uk_agentuser ADD mobile varchar(10) DEFAULT NULL COMMENT '是否移动端';


ALTER TABLE uk_chat_message ADD productid varchar(100) DEFAULT NULL COMMENT '商品id';
ALTER TABLE uk_chat_message ADD producttitle varchar(500) DEFAULT NULL COMMENT '商品标题';
ALTER TABLE uk_chat_message ADD productlogo varchar(500) DEFAULT NULL COMMENT '商品logo';
ALTER TABLE uk_chat_message ADD producturl varchar(500) DEFAULT NULL COMMENT '商品url';
ALTER TABLE uk_chat_message ADD productprice varchar(50) DEFAULT NULL COMMENT '商品价格';

CREATE TABLE `uk_tenant_type` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '租户ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `sort` int(11) DEFAULT 0 COMMENT '排序',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='租户分类表';


ALTER TABLE uk_contacts ADD jobname VARCHAR(255) DEFAULT NULL COMMENT '最近一次所属的任务调度的任务名';
ALTER TABLE uk_contacts ADD metaid VARCHAR(255) DEFAULT NULL COMMENT '表名';
ALTER TABLE uk_contacts ADD validresult VARCHAR(255) DEFAULT NULL COMMENT '名单是否有效';
ALTER TABLE uk_contacts ADD batid VARCHAR(255) DEFAULT NULL COMMENT '任务ID';
ALTER TABLE uk_contacts ADD taskid VARCHAR(255) DEFAULT NULL COMMENT '任务记录ID';

ALTER TABLE uk_contacts MODIFY COLUMN discount int COMMENT '分配次数';

ALTER TABLE uk_agentuser ADD usernum int DEFAULT 0 COMMENT '访客未读消息';

ALTER TABLE uk_tenant ADD validtime datetime DEFAULT NULL COMMENT '有效期';

ALTER TABLE uk_contacts ADD area VARCHAR(255) DEFAULT NULL COMMENT '区/县';
ALTER TABLE uk_contacts ADD cusarea VARCHAR(255) DEFAULT NULL COMMENT '客户-区/县';

ALTER TABLE uk_tenant ADD levels VARCHAR(255) DEFAULT NULL COMMENT '店铺等级';
ALTER TABLE uk_tenant ADD scorestars VARCHAR(255) DEFAULT NULL COMMENT '评价星级';
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880f66d1e548c016d1e634eaf005a', '平台', 'pub', 'platform', 'ukewo', 'layui-icon', '4028838b5b565caf015b566d11d80010', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-9-11 11:34:47', NULL, 1, 0, '4028838b5b565caf015b566d11d80010', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880f66d231890016d2422b1c807a8', '店铺类型', 'pub', 'com.dic.multitenant.category', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-9-12 14:21:56', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880f66d231890016d2425ff5c0806', '店铺分组', 'pub', 'com.dic.multitenant.grouping', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-9-12 14:25:33', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880f66d231890016d2428028d0842', '店铺评价星级', 'pub', 'com.dic.multitenant.scorestar', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-9-12 14:27:44', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880f66d231890016d24285c890853', '店铺等级', 'pub', 'com.dic.multitenant.level', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-9-12 14:28:07', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);


ALTER TABLE uk_sessionconfig ADD autoready TINYINT DEFAULT 0 COMMENT '坐席自动就绪';
ALTER TABLE uk_agentuser ADD membertype VARCHAR(255) DEFAULT NULL COMMENT '会员类型';
ALTER TABLE uk_agentservice ADD membertype VARCHAR(255) DEFAULT NULL COMMENT '会员类型';
ALTER TABLE uk_agentuser ADD custype VARCHAR(255) DEFAULT NULL COMMENT '多租户-客户类型';
ALTER TABLE uk_agentservice ADD custype VARCHAR(255) DEFAULT NULL COMMENT '多租户-客户类型';

ALTER TABLE uk_tenant ADD serviceweb VARCHAR(32) DEFAULT NULL COMMENT '平台服务网站';

ALTER TABLE uk_report ADD screen tinyint DEFAULT 0 COMMENT '启用投屏';
ALTER TABLE uk_report ADD norolling tinyint DEFAULT 0 COMMENT '禁用滚动';

ALTER TABLE uk_report ADD bgcolor VARCHAR(32) DEFAULT NULL COMMENT '背景颜色';
ALTER TABLE uk_report ADD bgimage VARCHAR(200) DEFAULT NULL COMMENT '背景图片';

ALTER TABLE uk_systemconfig ADD tenantserviceweb VARCHAR(32) DEFAULT NULL COMMENT '平台服务网站';

DELETE FROM uk_sysdic WHERE ID = '297e1e874f83129d014f833772a503ce';
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('4028800b6d4c35b4016d4c7b928300be', '延庆区', 'pub', '110118', 'ukewo', 'layui-icon', '297e1e874f83129d014f8334e3430242', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-09-20 10:23:49', NULL, 1, 52, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);

DELETE FROM uk_sysdic WHERE ID = '297e1e874f83129d014f8337729303cd';
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('4028800b6d4dd2b9016d4de5fef4001d', '密云区', 'pub', '110119', 'ukewo', 'layui-icon', '297e1e874f83129d014f8334e3430242', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-09-20 16:59:41', NULL, 1, 51, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);

DELETE FROM uk_sysdic WHERE ID = '297e1e874f83129d014f8337e99506e3';
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('4028800b6d5be908016d5beffe190062', '崇明区', 'pub', '310121', 'ukewo', 'layui-icon', '297e1e874f83129d014f8334e9e5028d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-09-23 10:25:17', NULL, 1, 835, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);


ALTER TABLE uk_publishedreport ADD screen tinyint DEFAULT 0 COMMENT '启用投屏';
ALTER TABLE uk_publishedreport ADD norolling tinyint DEFAULT 0 COMMENT '禁用滚动';

ALTER TABLE uk_publishedreport ADD bgcolor VARCHAR(32) DEFAULT NULL COMMENT '背景颜色';
ALTER TABLE uk_publishedreport ADD bgimage VARCHAR(200) DEFAULT NULL COMMENT '背景图片';

ALTER TABLE uk_systemconfig ADD monitor tinyint DEFAULT 0 COMMENT '是否启用智能监控';
ALTER TABLE uk_systemconfig ADD report tinyint DEFAULT 0 COMMENT '是否启用报表';
ALTER TABLE uk_systemconfig ADD asr tinyint DEFAULT 0 COMMENT '是否启用实时转写';
ALTER TABLE uk_systemconfig ADD channelshare tinyint DEFAULT 0 COMMENT '是否和软电话条共享通道';

ALTER TABLE uk_systemconfig ADD udp tinyint DEFAULT 0 COMMENT '是否启用UDP接收数据';
ALTER TABLE uk_systemconfig ADD udpstart int DEFAULT 0 COMMENT 'UDP起始端口';
ALTER TABLE uk_systemconfig ADD udpend int DEFAULT 0 COMMENT 'UDP结束端口';

ALTER TABLE uk_systemconfig ADD asrtype VARCHAR(50) DEFAULT NULL COMMENT 'ASR引擎类型';
ALTER TABLE uk_systemconfig ADD asrhost VARCHAR(100) DEFAULT NULL COMMENT 'ASR引擎地址';
ALTER TABLE uk_systemconfig ADD asrappid VARCHAR(50) DEFAULT NULL COMMENT 'ASR引擎APPID';
ALTER TABLE uk_systemconfig ADD asrappkey VARCHAR(100) DEFAULT NULL COMMENT 'ASR引擎APPKEY';
ALTER TABLE uk_systemconfig ADD asrappsec VARCHAR(100) DEFAULT NULL COMMENT 'ASR引擎APPSEC';
ALTER TABLE uk_systemconfig ADD asrcoderate VARCHAR(50) DEFAULT NULL COMMENT 'ASR引擎语音频率';

ALTER TABLE uk_tenant ADD externalid varchar(100) DEFAULT NULL COMMENT '外部id';

ALTER TABLE uk_systemconfig ADD vcode tinyint DEFAULT 0 COMMENT '是否启用验证码';

ALTER TABLE uk_systemconfig ADD remember tinyint DEFAULT 0 COMMENT '是否7天免验证登录';


ALTER TABLE uk_callcenter_event  MODIFY COLUMN contactsid VARCHAR(50) COMMENT '联系人ID';

ALTER TABLE uk_callcenter_event  MODIFY COLUMN dataid VARCHAR(50) COMMENT '数据ID';

ALTER TABLE uk_callcenter_pbxhost ADD orgtrans tinyint(4) DEFAULT 0 COMMENT '启用外呼转满意度';
ALTER TABLE uk_callcenter_pbxhost ADD orgtransdest varchar(50) DEFAULT NULL COMMENT '外呼转满意度号码';


ALTER TABLE uk_systemconfig ADD smshugup VARCHAR(50) DEFAULT NULL COMMENT '挂机短信模板ID';
ALTER TABLE uk_systemconfig ADD smsnoanswer VARCHAR(50) DEFAULT NULL COMMENT '呼入未接听短信模板ID';
ALTER TABLE uk_systemconfig ADD smsorgnoanswer VARCHAR(50) DEFAULT NULL COMMENT '呼出未接听短信模板ID';

ALTER TABLE uk_callcenter_pbxhost ADD enablesync tinyint(4) DEFAULT 0 COMMENT '启用自动同步呼叫中心数据';
ALTER TABLE uk_callcenter_pbxhost ADD enablesyncgw tinyint(4) DEFAULT 0 COMMENT '启用自动同步网关数据';

UPDATE `uk_templet` SET `NAME` = '数据表', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:18:43', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '<#if reportData??>\r\n<table class=\"layui-table ukefu-report\">\r\n  <thead>\r\n	<#if reportData?? && reportData.col?? && reportData.col.title??>\r\n	  <#list reportData.col.title as tlist>\r\n		<tr class=\"sortableTr\">\r\n		  <#if reportData.col.title?size gt 1 && tlist_index==0 && reportData.row.title?size gt 0>\r\n			<td align=\"center\" colspan=\"${reportData.row.title?size}\" rowspan=\"${reportData.col.title?size-1}\" class=\"s_blue\"></td>\r\n		  </#if>\r\n		  <#if (tlist_index+1)==reportData.col.title?size && reportData.row?? && reportData.row.firstTitle??>\r\n			<#list reportData.row.firstTitle as first>\r\n			  <td align=\"center\" data-title=\"${first.name?url}\" data-flag=\"dim\">${first.rename!first.name!\'\'}				\r\n			</#list>\r\n		  </#if>\r\n		  <#if tlist??>\r\n			<#list tlist as tl>\r\n			  <td align=\"center\" <#if tl.leveltype?? && tl.leveltype==\"newcol\">rowspan=\"${reportData.col.title?size}\"</#if>colspan=\"${tl.colspan}\" > ${tl.rename!tl.name!\"\"}\r\n				</td>\r\n			</#list>\r\n			</#if>\r\n	</tr>\r\n	</#list>\r\n	</#if>\r\n  </thead>\r\n  <tbody>\r\n	<#if reportData?? && reportData.data??>\r\n	  <#list reportData.data as values>\r\n		<tr class=\"rowcell\">\r\n		  <#if reportData.row?? && reportData.row.title?? && reportData.row.title?size gt 0>\r\n			<#list reportData.row.title as tl>\r\n			  <#assign rows=0>\r\n				<#list tl as title>\r\n				  <#if title??>\r\n					<#if rows==values_index && title.name !=\"TOTAL_TEMP\">\r\n					  	<#if title.leveltype !=\"newrow_extend_sum\">\r\n						<td nowrap=\"nowrap\" align=\"center\" class=\"blue_k <#if title.total == true>total</#if>\" rowspan=\"${title.rowspan!\'0\'}\" <#if title.colspan gt 1>colspan=\"${title.colspan}\"</#if>>\r\n						  ${title.formatName!\'\'}</td>\r\n						</#if>\r\n						<#if title.valueData??>\r\n						  <#list title.valueData as value>\r\n							<#if value.merge==false>\r\n							  <td rowspan=\"${value.rowspan}\" colspan=\"${value.colspan}\" align=\"center\" class=\"measure ${value.vtclass!\'\'}\" nowrap=\"nowrap\" <#if value.cellmergeid??>data-cellmerge=\"${value.cellmergeid}\"</#if>>\r\n								<#if value.foramatValue?? && value.foramatValue != \'\'>\r\n								${value.valueStyle!value.foramatValue!\'\'}\r\n								<#else>\r\n								0\r\n								</#if>\r\n							\r\n							</td>\r\n							</#if>\r\n							</#list>\r\n						  </#if>\r\n					  </#if>\r\n					  <#assign rows=rows + title.rowspan>\r\n				  </#if>\r\n			</#list>\r\n	</#list>\r\n	<#else>\r\n	  <#list values as value>\r\n		<td class=\"row \" style=\"text-align: right;<#if value.valueType?? && value.valueType == \'total\'>background-color:#c5daed;</#if>\">\r\n		  <#if value.foramatValue?? && value.foramatValue != \'\'>\r\n								${value.valueStyle!value.foramatValue!\'\'}\r\n								<#else>\r\n								0\r\n								</#if>\r\n		  \r\n		  </td>\r\n	  </#list>\r\n	  </#if>\r\n	  </tr>\r\n	  </#list>\r\n	  </#if>\r\n  </tbody>\r\n</table>\r\n<#if reportModel?? && reportModel.isloadfulldata == \'true\'>\r\n<#if reportData?? && reportData.total gt reportData.pageSize>\r\n<div class=\"row\" style=\"padding:5px;\">\r\n	<div class=\"col-lg-12\" id=\"page_${reportModel.id!\'\'}\" style=\"text-align:center;\"></div>\r\n</div>\r\n</#if>\r\n<script>\r\n	layui.use([\'laypage\', \'layer\',\'form\'], function(){\r\n		  var laypage = layui.laypage\r\n		  ,layer = layui.layer;\r\n		  laypage.render({\r\n			  elem:\'page_${reportModel.id!\'\'}\'\r\n				,count: <#if reportData??>${reportData.total}<#else>0</#if> \r\n				,curr:<#if reportData??>${reportData.page}<#else>0</#if>\r\n				,groups: 5 \r\n				,limit:<#if reportData?? && reportData.pageSize gt 0>${reportData.pageSize}<#else>20</#if>\r\n				,jump:function(data , first){\r\n					if(!first){\r\n						loadURL(\"/apps/report/design/getelement.html?id=<#if reportModel??>${reportModel.id!\'\'}</#if>&p=\"+data.curr,\"#${reportModel.id!\'\'}\");\r\n					}\r\n				}\r\n		   });\r\n		});\r\n</script>\r\n</#if>\r\n<#else>\r\n<div class=\"layui-form\">\r\n  <table class=\"layui-table\">\r\n    <thead>\r\n      <tr><th>标题一</th><th>标题二</th><th>标题三</th></tr> \r\n    </thead>\r\n    <tbody>\r\n      <tr><td>样例数据</td><td>样例数据</td><td>样例数据</td>\r\n      </tr>\r\n      <tr><td>样例数据</td><td>样例数据</td><td>样例数据</td></tr>\r\n      <tr><td>样例数据</td><td>样例数据</td><td>样例数据</td></tr>\r\n    </tbody>\r\n  </table>\r\n</div>\r\n</#if>', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/table.png', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = 'simple', `charttype` = NULL WHERE `ID` = '4028811b6191e289016191e4721c033d';
UPDATE `uk_templet` SET `NAME` = '金字塔图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:21:57', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n	var data_${element.mid!\'\'} = [];\r\n		<#if reportData.col??>\r\n			<#list reportData.col.title as title>\r\n				<#if (title_index+1) == reportData.col.title?size>\r\n					<#list title as rowtl>\r\n						var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"key\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n						\r\n								<#if reportData?? && reportData.data??>\r\n									<#list reportData.data as values>\r\n										\r\n											<#list values as val>	\r\n										\r\n												<#if rowtl.name == val.name>\r\n													\r\n													\r\n													<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n														obj_${element.mid!\'\'}[\'value\'] = ${val.valueStyle!val.foramatValue!\'\'}\r\n													<#else>\r\n														obj_${element.mid!\'\'}[\'value\'] = 0\r\n													</#if>\r\n													\r\n												</#if>\r\n												\r\n											</#list>\r\n										\r\n									</#list>\r\n								</#if>\r\n						\r\n						data_${element.mid!\'\'}.push(obj_${element.mid!\'\'})						\r\n					</#list>			\r\n				</#if>\r\n			</#list>\r\n		</#if>\r\n		<#else>\r\n		const data_${element.mid!\'\'} = [\r\n					{ key: \'浏览网站\', value: 50000 },\r\n					{ key: \'放入购物车\', value: 35000 },\r\n					{ key: \'生成订单\', value: 25000 },\r\n					{ key: \'支付订单\', value: 15000 },\r\n					{ key: \'完成交易\', value: 8000 }\r\n				  ];		\r\n		</#if>	\r\n	  const dv_${element.mid!\'\'} = data_${element.mid!\'\'};\r\n	var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  \r\n	  console.info(data_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/jzt.png', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'pyramid' WHERE `ID` = '4028811b6191e289016191e76714033e';
UPDATE `uk_templet` SET `NAME` = '漏斗图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:25:01', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n	var data_${element.mid!\'\'} = [];\r\n		<#if reportData.col??>\r\n			<#list reportData.col.title as title>\r\n				<#if (title_index+1) == reportData.col.title?size>\r\n					<#list title as rowtl>\r\n						var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"key\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n						\r\n									<#if reportData?? && reportData.data??>\r\n										<#list reportData.data as values>\r\n											\r\n												<#list values as val>	\r\n													\r\n													<#if rowtl.name == val.name>\r\n														\r\n														\r\n														<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n															obj_${element.mid!\'\'}[\'value\'] = ${val.valueStyle!val.foramatValue!\'\'}\r\n														<#else>\r\n															obj_${element.mid!\'\'}[\'value\'] = 0\r\n														</#if>\r\n														\r\n													</#if>\r\n												</#list>\r\n											\r\n										</#list>\r\n									</#if>\r\n						\r\n						data_${element.mid!\'\'}.push(obj_${element.mid!\'\'})						\r\n					</#list>			\r\n				</#if>\r\n			</#list>\r\n		</#if>\r\n		<#else>\r\n		const data_${element.mid!\'\'} = [\r\n					{ key: \'浏览网站\', value: 50000 ,color:\'#ffffff\'},\r\n					{ key: \'放入购物车\', value: 35000 },\r\n					{ key: \'生成订单\', value: 25000 },\r\n					{ key: \'支付订单\', value: 15000 },\r\n					{ key: \'完成交易\', value: 8000 }\r\n				  ];		\r\n		</#if>	\r\n	  const dv_${element.mid!\'\'} = data_${element.mid!\'\'};\r\n	var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  \r\n	  console.info(data_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/loudou.png', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'funnel' WHERE `ID` = '4028811b6191e289016191ea38620340';
UPDATE `uk_templet` SET `NAME` = '折线图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:26:34', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n		<#if reportData.row?? && reportData.row.title?size gt 0 && reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n			<#list reportData.row.title as tl>\r\n				<#assign inx = 0>\r\n				<#list tl as title>\r\n					var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"_name\"] = \"${title.name!\'\'}\";\r\n						<#if title.valueData??>\r\n							<#list title.valueData as val>\r\n								\r\n								<#if val.valueStyle!val.foramatValue?? && val.valueStyle!val.foramatValue != \'\'>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'};\r\n								<#else>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = 0;\r\n								</#if>\r\n									<#if inx == 0>\r\n										mea_${element.mid!\'\'}.push(\'${val.name!\'\'}\');	\r\n									</#if>\r\n								\r\n							</#list>\r\n						</#if>\r\n					<#assign inx = inx + 1>		\r\n					data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});							\r\n				</#list>\r\n					\r\n			</#list>\r\n\r\n		<#elseif reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n				<#list reportData.col.title as title>\r\n					<#if (title_index+1) == reportData.col.title?size>\r\n							\r\n						<#list title as rowtl>	\r\n										var obj_${element.mid!\'\'} = new Object();\r\n										obj_${element.mid!\'\'}[\"_name\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n																\r\n										<#if reportData?? && reportData.data??>\r\n											<#list reportData.data as values>\r\n												\r\n													<#list values as val>	\r\n														<#if rowtl_index == val_index>\r\n																\r\n															\r\n															<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'}\r\n															<#else>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = 0\r\n															</#if>\r\n															\r\n														</#if>\r\n													</#list>\r\n												\r\n											</#list>\r\n										</#if>\r\n										\r\n										data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});						\r\n											\r\n							mea_${element.mid!\'\'}.push(\'${rowtl.rename!rowtl.name!\'\'}\');						\r\n						</#list>\r\n					</#if>\r\n				</#list>\r\n			\r\n		<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n		</#if>\r\n		\r\n	<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n	</#if>\r\n	\r\n		const ds_${element.mid!\'\'} = new DataSet();\r\n	  const dv_${element.mid!\'\'} = ds_${element.mid!\'\'}.createView().source(data_${element.mid!\'\'});\r\n	  dv_${element.mid!\'\'}.transform({\r\n		type: \'fold\',\r\n		fields: mea_${element.mid!\'\'}, // 展开字段集\r\n		key: \'key\', // key字段\r\n		value: \'value\', // value字段\r\n	  });\r\n		var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  	  	console.info(data_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/line.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'line' WHERE `ID` = '4028811b6191e289016191eba2190341';
UPDATE `uk_templet` SET `NAME` = '柱形图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:26:56', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n		<#if reportData.row?? && reportData.row.title?size gt 0 && reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n			<#list reportData.row.title as tl>\r\n				<#assign inx = 0>\r\n				<#list tl as title>\r\n					var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"_name\"] = \"${title.name!\'\'}\";\r\n						<#if title.valueData??>\r\n							<#list title.valueData as val>\r\n								\r\n								<#if val.valueStyle!val.foramatValue?? && val.valueStyle!val.foramatValue != \'\'>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'};\r\n								<#else>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = 0;\r\n								</#if>\r\n									<#if inx == 0>\r\n										mea_${element.mid!\'\'}.push(\'${val.name!\'\'}\');	\r\n									</#if>\r\n								\r\n							</#list>\r\n						</#if>\r\n					<#assign inx = inx + 1>		\r\n					data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});							\r\n				</#list>\r\n					\r\n			</#list>\r\n\r\n		<#elseif reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n				<#list reportData.col.title as title>\r\n					<#if (title_index+1) == reportData.col.title?size>\r\n							\r\n						<#list title as rowtl>	\r\n										var obj_${element.mid!\'\'} = new Object();\r\n										obj_${element.mid!\'\'}[\"_name\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n																\r\n										<#if reportData?? && reportData.data??>\r\n											<#list reportData.data as values>\r\n												\r\n													<#list values as val>	\r\n														<#if rowtl_index == val_index>\r\n																\r\n															\r\n															<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'}\r\n															<#else>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = 0\r\n															</#if>\r\n															\r\n														</#if>\r\n													</#list>\r\n												\r\n											</#list>\r\n										</#if>\r\n										\r\n										data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});						\r\n											\r\n							mea_${element.mid!\'\'}.push(\'${rowtl.rename!rowtl.name!\'\'}\');						\r\n						</#list>\r\n					</#if>\r\n				</#list>\r\n			\r\n		<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n		</#if>\r\n		\r\n	<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n	</#if>\r\n	\r\n		const ds_${element.mid!\'\'} = new DataSet();\r\n	  const dv_${element.mid!\'\'} = ds_${element.mid!\'\'}.createView().source(data_${element.mid!\'\'});\r\n	  dv_${element.mid!\'\'}.transform({\r\n		type: \'fold\',\r\n		fields: mea_${element.mid!\'\'}, // 展开字段集\r\n		key: \'key\', // key字段\r\n		value: \'value\', // value字段\r\n	  });\r\n		var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  	  	console.info(data_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/bar.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'histogram' WHERE `ID` = '4028811b6191e289016191ebf9ae0342';
UPDATE `uk_templet` SET `NAME` = '饼形图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:27:13', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n	var data_${element.mid!\'\'} = [];\r\n	var field_${element.mid!\'\'}  = \"\";\r\n	<#if reportData.col??>\r\n		<#list reportData.col.title as title>\r\n			<#if (title_index+1) == reportData.col.title?size>\r\n				<#list title as rowtl>	\r\n					var obj_${element.mid!\'\'} = new Object();\r\n					obj_${element.mid!\'\'}[\"key\"] = \'${rowtl.rename!rowtl.name!\'\'}\';\r\n					\r\n									<#list reportData.data as values>\r\n										\r\n											<#list values as val>\r\n											\r\n												<#if rowtl.name == val.name>\r\n													\r\n													<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n														obj_${element.mid!\'\'}[\'value\'] = ${val.valueStyle!val.foramatValue!\'\'}\r\n													<#else>\r\n														obj_${element.mid!\'\'}[\'value\'] = 0\r\n													</#if>\r\n													\r\n													\r\n													\r\n													\r\n												</#if>\r\n												\r\n											</#list>\r\n										\r\n									</#list>									\r\n																										\r\n						\r\n					data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});		\r\n				</#list>										\r\n			</#if>\r\n		</#list>\r\n	</#if>\r\n	<#else>\r\n		const data_${element.mid!\'\'} = [\r\n			{ key: \'事例一\', value: 40 },\r\n			{ key: \'事例二\', value: 21 },\r\n			{ key: \'事例三\', value: 17 },\r\n			{ key: \'事例四\', value: 13 },\r\n			{ key: \'事例五\', value: 9 }\r\n		  ];\r\n		 \r\n	</#if>\r\n	var field_${element.mid!\'\'}  = \"value\";\r\n		console.info(data_${element.mid!\'\'})\r\n		console.info(field_${element.mid!\'\'})\r\n		const { DataView } = DataSet;\r\n	  const dv_${element.mid!\'\'} = new DataView();\r\n	  dv_${element.mid!\'\'}.source(data_${element.mid!\'\'}).transform({\r\n		type: \'percent\',\r\n		field: field_${element.mid!\'\'},\r\n		dimension: \'key\',\r\n		as: \'value\'\r\n	  });\r\n	   var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  console.info(data_${element.mid!\'\'})\r\n	</script>', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/pie.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'pie' WHERE `ID` = '4028811b6191e289016191ec3c900343';
UPDATE `uk_templet` SET `NAME` = '面积图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:27:35', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n		<#if reportData.row?? && reportData.row.title?size gt 0 && reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n			<#list reportData.row.title as tl>\r\n				<#assign inx = 0>\r\n				<#list tl as title>\r\n					var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"_name\"] = \"${title.name!\'\'}\";\r\n						<#if title.valueData??>\r\n							<#list title.valueData as val>\r\n								\r\n								<#if val.valueStyle!val.foramatValue?? && val.valueStyle!val.foramatValue != \'\'>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'};\r\n								<#else>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = 0;\r\n								</#if>\r\n									<#if inx == 0>\r\n										mea_${element.mid!\'\'}.push(\'${val.name!\'\'}\');	\r\n									</#if>\r\n								\r\n							</#list>\r\n						</#if>\r\n					<#assign inx = inx + 1>		\r\n					data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});							\r\n				</#list>\r\n					\r\n			</#list>\r\n\r\n		<#elseif reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n				<#list reportData.col.title as title>\r\n					<#if (title_index+1) == reportData.col.title?size>\r\n							\r\n						<#list title as rowtl>	\r\n										var obj_${element.mid!\'\'} = new Object();\r\n										obj_${element.mid!\'\'}[\"_name\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n																\r\n										<#if reportData?? && reportData.data??>\r\n											<#list reportData.data as values>\r\n												\r\n													<#list values as val>	\r\n														<#if rowtl_index == val_index>\r\n																\r\n															\r\n															<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'}\r\n															<#else>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = 0\r\n															</#if>\r\n															\r\n														</#if>\r\n													</#list>\r\n												\r\n											</#list>\r\n										</#if>\r\n										\r\n										data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});						\r\n											\r\n							mea_${element.mid!\'\'}.push(\'${rowtl.rename!rowtl.name!\'\'}\');						\r\n						</#list>\r\n					</#if>\r\n				</#list>\r\n			\r\n		<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n		</#if>\r\n		\r\n	<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n	</#if>\r\n	\r\n		const ds_${element.mid!\'\'} = new DataSet();\r\n	  const dv_${element.mid!\'\'} = ds_${element.mid!\'\'}.createView().source(data_${element.mid!\'\'});\r\n	  dv_${element.mid!\'\'}.transform({\r\n		type: \'fold\',\r\n		fields: mea_${element.mid!\'\'}, // 展开字段集\r\n		key: \'key\', // key字段\r\n		value: \'value\', // value字段\r\n	  });\r\n		var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  	  	console.info(data_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/area.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'area' WHERE `ID` = '4028811b6191e289016191ec901f0344';
UPDATE `uk_templet` SET `NAME` = '条形图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:28:07', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n		<#if reportData.row?? && reportData.row.title?size gt 0 && reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n			<#list reportData.row.title as tl>\r\n				<#assign inx = 0>\r\n				<#list tl as title>\r\n					var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"_name\"] = \"${title.name!\'\'}\";\r\n						<#if title.valueData??>\r\n							<#list title.valueData as val>\r\n								\r\n								<#if val.valueStyle!val.foramatValue?? && val.valueStyle!val.foramatValue != \'\'>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'};\r\n								<#else>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = 0;\r\n								</#if>\r\n									<#if inx == 0>\r\n										mea_${element.mid!\'\'}.push(\'${val.name!\'\'}\');	\r\n									</#if>\r\n								\r\n							</#list>\r\n						</#if>\r\n					<#assign inx = inx + 1>		\r\n					data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});							\r\n				</#list>\r\n					\r\n			</#list>\r\n\r\n		<#elseif reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n				<#list reportData.col.title as title>\r\n					<#if (title_index+1) == reportData.col.title?size>\r\n							\r\n						<#list title as rowtl>	\r\n										var obj_${element.mid!\'\'} = new Object();\r\n										obj_${element.mid!\'\'}[\"_name\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n																\r\n										<#if reportData?? && reportData.data??>\r\n											<#list reportData.data as values>\r\n												\r\n													<#list values as val>	\r\n														<#if rowtl_index == val_index>\r\n																\r\n															\r\n															<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'}\r\n															<#else>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = 0\r\n															</#if>\r\n															\r\n														</#if>\r\n													</#list>\r\n												\r\n											</#list>\r\n										</#if>\r\n										\r\n										data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});						\r\n											\r\n							mea_${element.mid!\'\'}.push(\'${rowtl.rename!rowtl.name!\'\'}\');						\r\n						</#list>\r\n					</#if>\r\n				</#list>\r\n			\r\n		<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n		</#if>\r\n		\r\n	<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n	</#if>\r\n	\r\n		const ds_${element.mid!\'\'} = new DataSet();\r\n	  const dv_${element.mid!\'\'} = ds_${element.mid!\'\'}.createView().source(data_${element.mid!\'\'});\r\n	  dv_${element.mid!\'\'}.transform({\r\n		type: \'fold\',\r\n		fields: mea_${element.mid!\'\'}, // 展开字段集\r\n		key: \'key\', // key字段\r\n		value: \'value\', // value字段\r\n	  });\r\n		var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  	  	console.info(data_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/flat.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'bar' WHERE `ID` = '4028811b6191e289016191ed0e160345';
UPDATE `uk_templet` SET `NAME` = '环形图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:28:21', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n	var data_${element.mid!\'\'} = [];\r\n	var field_${element.mid!\'\'}  = \"\";\r\n	<#if reportData.col??>\r\n		<#list reportData.col.title as title>\r\n			<#if (title_index+1) == reportData.col.title?size>\r\n				<#list title as rowtl>	\r\n					var obj_${element.mid!\'\'} = new Object();\r\n					obj_${element.mid!\'\'}[\"key\"] = \'${rowtl.rename!rowtl.name!\'\'}\';\r\n					\r\n								\r\n									<#list reportData.data as values>\r\n										\r\n											<#list values as val>	\r\n												<#if rowtl.name == val.name>\r\n													\r\n													\r\n													<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n														obj_${element.mid!\'\'}[\'value\'] = ${val.valueStyle!val.foramatValue!\'\'}\r\n													<#else>\r\n														obj_${element.mid!\'\'}[\'value\'] = 0\r\n													</#if>\r\n													\r\n												</#if>\r\n											</#list>\r\n										\r\n									</#list>									\r\n																										\r\n							\r\n					data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});		\r\n				</#list>										\r\n			</#if>\r\n		</#list>\r\n	</#if>\r\n	<#else>\r\n		const data_${element.mid!\'\'} = [\r\n			{ key: \'事例一\', value: 40 },\r\n			{ key: \'事例二\', value: 21 },\r\n			{ key: \'事例三\', value: 17 },\r\n			{ key: \'事例四\', value: 13 },\r\n			{ key: \'事例五\', value: 9 }\r\n		  ];\r\n		  \r\n	</#if>\r\n	var field_${element.mid!\'\'}  = \"value\";\r\n		const { DataView } = DataSet;\r\n	  const dv_${element.mid!\'\'} = new DataView();\r\n	  dv_${element.mid!\'\'}.source(data_${element.mid!\'\'}).transform({\r\n		type: \'percent\',\r\n		field: field_${element.mid!\'\'},\r\n		dimension: \'key\',\r\n		as: \'value\'\r\n	  });\r\n	   var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  console.info(data_${element.mid!\'\'})\r\n	</script>', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/hole.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'ring' WHERE `ID` = '4028811b6191e289016191ed46550346';
UPDATE `uk_templet` SET `NAME` = '点状图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:28:42', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n	var data_${element.mid!\'\'} = [];\r\n		<#if reportData.col??>\r\n			<#list reportData.col.title as title>\r\n				<#if (title_index+1) == reportData.col.title?size>\r\n					<#list title as rowtl>\r\n						var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"_name\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n						\r\n								<#if reportData?? && reportData.data??>\r\n									<#list reportData.data as values>\r\n										\r\n											<#list values as val>	\r\n										\r\n												<#if rowtl.name == val.name>\r\n													obj_${element.mid!\'\'}[\'key\'] = ${val.valueStyle!val.foramatValue!\'\'}\r\n													\r\n													\r\n													<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n														obj_${element.mid!\'\'}[\'value\'] = ${val.valueStyle!val.foramatValue!\'\'}\r\n													<#else>\r\n														obj_${element.mid!\'\'}[\'value\'] = 0\r\n													</#if>\r\n													\r\n												</#if>\r\n												\r\n											</#list>\r\n										\r\n									</#list>\r\n								</#if>\r\n							\r\n						data_${element.mid!\'\'}.push(obj_${element.mid!\'\'})						\r\n					</#list>			\r\n				</#if>\r\n			</#list>\r\n		</#if>\r\n		<#else>\r\n		const data_${element.mid!\'\'} = [{\"_name\":\"female\",\"key\":161.2,\"value\":51.6},{\"_name\":\"female\",\"key\":167.5,\"value\":59},{\"_name\":\"female\",\"key\":159.5,\"value\":49.2},{\"_name\":\"female\",\"key\":157,\"value\":63},{\"_name\":\"female\",\"key\":155.8,\"value\":53.6},{\"_name\":\"female\",\"key\":170,\"value\":59},{\"_name\":\"female\",\"key\":159.1,\"value\":47.6},{\"_name\":\"female\",\"key\":166,\"value\":69.8},{\"_name\":\"female\",\"key\":176.2,\"value\":66.8},{\"_name\":\"female\",\"key\":160.2,\"value\":75.2},{\"_name\":\"female\",\"key\":172.5,\"value\":55.2},{\"_name\":\"female\",\"key\":170.9,\"value\":54.2},{\"_name\":\"female\",\"key\":172.9,\"value\":62.5},{\"_name\":\"female\",\"key\":153.4,\"value\":42},{\"_name\":\"female\",\"key\":160,\"value\":50},{\"_name\":\"female\",\"key\":147.2,\"value\":49.8},{\"_name\":\"female\",\"key\":168.2,\"value\":49.2},{\"_name\":\"female\",\"key\":175,\"value\":73.2},{\"_name\":\"female\",\"key\":157,\"value\":47.8},{\"_name\":\"female\",\"key\":167.6,\"value\":68.8},{\"_name\":\"female\",\"key\":159.5,\"value\":50.6},{\"_name\":\"female\",\"key\":175,\"value\":82.5},{\"_name\":\"female\",\"key\":166.8,\"value\":57.2},{\"_name\":\"female\",\"key\":176.5,\"value\":87.8},{\"_name\":\"female\",\"key\":170.2,\"value\":72.8},{\"_name\":\"female\",\"key\":174,\"value\":54.5},{\"_name\":\"female\",\"key\":173,\"value\":59.8},{\"_name\":\"female\",\"key\":179.9,\"value\":67.3},{\"_name\":\"female\",\"key\":170.5,\"value\":67.8},{\"_name\":\"female\",\"key\":160,\"value\":47},{\"_name\":\"female\",\"key\":154.4,\"value\":46.2},{\"_name\":\"female\",\"key\":162,\"value\":55},{\"_name\":\"female\",\"key\":176.5,\"value\":83},{\"_name\":\"female\",\"key\":160,\"value\":54.4},{\"_name\":\"female\",\"key\":152,\"value\":45.8},{\"_name\":\"female\",\"key\":162.1,\"value\":53.6},{\"_name\":\"female\",\"key\":170,\"value\":73.2},{\"_name\":\"male\",\"key\":160.2,\"value\":52.1},{\"_name\":\"female\",\"key\":161.3,\"value\":67.9}]		\r\n		</#if>	\r\n	  const dv_${element.mid!\'\'} = data_${element.mid!\'\'};\r\n	var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/point.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'dotplot' WHERE `ID` = '4028811b6191e289016191ed97a80347';
UPDATE `uk_templet` SET `NAME` = '雷达图', `DESCRIPTION` = NULL, `CODE` = 'report', `GROUPID` = NULL, `CREATETIME` = '2018-02-14 09:28:54', `USERID` = NULL, `TEMPLETTITLE` = NULL, `TEMPLETTEXT` = '	<script>\r\n	<#if reportData??>\r\n		<#if reportData.row?? && reportData.row.title?size gt 0 && reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n			<#list reportData.row.title as tl>\r\n				<#assign inx = 0>\r\n				<#list tl as title>\r\n					var obj_${element.mid!\'\'} = new Object();\r\n						obj_${element.mid!\'\'}[\"_name\"] = \"${title.name!\'\'}\";\r\n						<#if title.valueData??>\r\n							<#list title.valueData as val>\r\n								\r\n								<#if val.valueStyle!val.foramatValue?? && val.valueStyle!val.foramatValue != \'\'>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'};\r\n								<#else>\r\n									obj_${element.mid!\'\'}[\"${val.name!\'\'}\"] = 0;\r\n								</#if>\r\n									<#if inx == 0>\r\n										mea_${element.mid!\'\'}.push(\'${val.name!\'\'}\');	\r\n									</#if>\r\n								\r\n							</#list>\r\n						</#if>\r\n					<#assign inx = inx + 1>		\r\n					data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});							\r\n				</#list>\r\n					\r\n			</#list>\r\n\r\n		<#elseif reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>\r\n			var data_${element.mid!\'\'} = [];\r\n			var mea_${element.mid!\'\'} = [];\r\n				<#list reportData.col.title as title>\r\n					<#if (title_index+1) == reportData.col.title?size>\r\n							\r\n						<#list title as rowtl>	\r\n										var obj_${element.mid!\'\'} = new Object();\r\n										obj_${element.mid!\'\'}[\"_name\"] = \"${rowtl.rename!rowtl.name!\'\'}\";\r\n																\r\n										<#if reportData?? && reportData.data??>\r\n											<#list reportData.data as values>\r\n												\r\n													<#list values as val>	\r\n														<#if rowtl_index == val_index>\r\n														\r\n															\r\n															<#if val.foramatValue?? && val.foramatValue != \'\'>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = ${val.valueStyle!val.foramatValue!\'\'}\r\n															<#else>\r\n																obj_${element.mid!\'\'}[\"${rowtl.rename!rowtl.name!\'\'}\"] = 0\r\n															</#if>\r\n															\r\n														</#if>\r\n													</#list>\r\n												\r\n											</#list>\r\n										</#if>\r\n										\r\n										data_${element.mid!\'\'}.push(obj_${element.mid!\'\'});						\r\n											\r\n							mea_${element.mid!\'\'}.push(\'${rowtl.rename!rowtl.name!\'\'}\');						\r\n						</#list>\r\n					</#if>\r\n				</#list>\r\n			\r\n		<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n		</#if>\r\n		\r\n	<#else>\r\n\r\n		const data_${element.mid!\'\'} = [\r\n			{ _name:\'London\', \'Jan.\': 18.9, \'Feb.\': 28.8, \'Mar.\' :39.3, \'Apr.\': 81.4, \'May\': 47, \'Jun.\': 20.3, \'Jul.\': 24, \'Aug.\': 35.6 },\r\n			{ _name:\'Berlin\', \'Jan.\': 12.4, \'Feb.\': 23.2, \'Mar.\' :34.5, \'Apr.\': 99.7, \'May\': 52.6, \'Jun.\': 35.5, \'Jul.\': 37.4, \'Aug.\': 42.4}\r\n		  ];\r\n		const mea_${element.mid!\'\'} =  [ \'Jan.\',\'Feb.\',\'Mar.\',\'Apr.\',\'May\',\'Jun.\',\'Jul.\',\'Aug.\' ];\r\n	</#if>\r\n	\r\n		const ds_${element.mid!\'\'} = new DataSet();\r\n	  const dv_${element.mid!\'\'} = ds_${element.mid!\'\'}.createView().source(data_${element.mid!\'\'});\r\n	  dv_${element.mid!\'\'}.transform({\r\n		type: \'fold\',\r\n		fields: mea_${element.mid!\'\'}, // 展开字段集\r\n		key: \'key\', // key字段\r\n		value: \'value\', // value字段\r\n	  });\r\n		var json_${element.mid!\'\'} =  $.parseJSON( $(\"#json_${element.mid!\'\'}\").text() );\r\n	  $(\"#json_${element.mid!\'\'}\").remove();\r\n	  ChartAction.renderChart(\'${element.mid!\'\'}\',dv_${element.mid!\'\'},json_${element.mid!\'\'})\r\n	  	  	console.info(data_${element.mid!\'\'})\r\n	</script>\r\n', `TEMPLETTYPE` = '4028811b618d0dca01618d5a5fe6034a', `ORGI` = 'ukewo', `ICONSTR` = '/images/design/radar.gif', `MEMO` = NULL, `ORDERINDEX` = NULL, `TYPEID` = NULL, `SELDATA` = NULL, `layoutcols` = 0, `datatype` = NULL, `charttype` = 'radar' WHERE `ID` = '4028811b6191e289016191edc7b50348';

ALTER TABLE uk_weixinuser ADD appid VARCHAR(50) DEFAULT NULL COMMENT '微信公众号唯一标识';


ALTER TABLE uk_callcenter_extention ADD outnum varchar(100) DEFAULT NULL COMMENT '外显号码';

ALTER TABLE uk_systemconfig ADD enablesecurity tinyint(4) DEFAULT '0' COMMENT '启用安全性';
ALTER TABLE uk_systemconfig ADD securitypws varchar(200) DEFAULT NULL COMMENT '安全性密码';
ALTER TABLE uk_systemconfig ADD securitypublickey text DEFAULT NULL COMMENT '安全性公钥';
ALTER TABLE uk_systemconfig ADD securityprivatekey text DEFAULT NULL COMMENT '安全性私钥';


ALTER TABLE uk_callcenter_pbxhost ADD enableacl tinyint DEFAULT 0 COMMENT '是否启用通话拦截';
ALTER TABLE uk_callcenter_pbxhost ADD enablereplace tinyint DEFAULT 0 COMMENT '是否启用字符串替换';
ALTER TABLE uk_callcenter_pbxhost ADD replacestr VARCHAR(200) DEFAULT NULL COMMENT '字符替换规则';
ALTER TABLE uk_callcenter_pbxhost ADD replacereg VARCHAR(200) DEFAULT NULL COMMENT '替换字符';


ALTER TABLE uk_callcenter_siptrunk ADD incount int DEFAULT 0 COMMENT '呼入计数';
ALTER TABLE uk_callcenter_siptrunk ADD outcount int DEFAULT 0 COMMENT '呼出计数';
ALTER TABLE uk_callcenter_siptrunk ADD totalcount int DEFAULT 0 COMMENT '全部计数';
ALTER TABLE uk_callcenter_siptrunk ADD outlimit int DEFAULT 0 COMMENT '呼出限流';
ALTER TABLE uk_callcenter_siptrunk ADD enablenear tinyint DEFAULT 0 COMMENT '启用匹配被叫号码和主叫号码归属地';

ALTER TABLE uk_callcenter_siptrunk ADD profile VARCHAR(20) DEFAULT "internal" COMMENT '配置文件';

ALTER TABLE uk_callcenter_siptrunk ADD ims tinyint DEFAULT 0 COMMENT '移动IMS网络';
ALTER TABLE uk_callcenter_siptrunk ADD siptype tinyint DEFAULT 0 COMMENT 'SIP类型';

ALTER TABLE uk_callcenter_event ADD gateway VARCHAR(100) DEFAULT NULL COMMENT '网关名称';
ALTER TABLE uk_callcenter_event ADD gatewaycount tinyint DEFAULT 0 COMMENT '网关已经计数';

CREATE INDEX index_301 ON uk_callcenter_event  (`gateway`) USING BTREE; 

CREATE INDEX index_302 ON uk_callcenter_event  (`gatewaycount`) USING BTREE; 

ALTER TABLE uk_number_pool ADD siptrunkid VARCHAR(50) DEFAULT NULL COMMENT '所属SIP网关';
ALTER TABLE uk_number_pool ADD province VARCHAR(255) DEFAULT NULL COMMENT '所属省份';
ALTER TABLE uk_number_pool ADD city VARCHAR(255) DEFAULT NULL COMMENT '所属城市';
ALTER TABLE uk_callcenter_siptrunk ADD resetcount tinyint DEFAULT 0 COMMENT '重置通话计数（incount，outcount，totalcount置为0）';

ALTER TABLE uk_que_survey_question ADD smsid VARCHAR(50) DEFAULT NULL COMMENT '短信模板ID';
ALTER TABLE uk_que_survey_question ADD enablesms tinyint DEFAULT 0 COMMENT '启用发送短信';

UPDATE `uk_sysdic` SET `NAME` = '菏泽市', `TITLE` = 'pub', `CODE` = '371700', `ORGI` = 'ukewo', `CTYPE` = 'layui-icon', `PARENTID` = '297e1e874f83129d014f832155a500d1', `DESCRIPTION` = '', `MEMO` = NULL, `ICONSTR` = '', `ICONSKIN` = '', `CATETYPE` = NULL, `CREATER` = '297e8c7b455798280145579c73e501c1', `CREATETIME` = '2015-08-31 18:02:09', `UPDATETIME` = '2015-08-31 18:02:09', `HASCHILD` = 0, `SORTINDEX` = 188, `DICID` = '297e1e874f83129d014f832090e200b8', `DEFAULTVALUE` = 0, `DISCODE` = 0, `URL` = NULL, `MODULE` = NULL, `MLEVEL` = NULL, `RULES` = NULL, `MENUTYPE` = NULL, `tenant` = 0 WHERE `ID` = '297e1e874f83129d014f8334f40802dc';
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e96e3ac4dd016e3ac8430e003d', '外呼机器人', 'pub', 'salespatter', 'ukewo', 'layui-icon', '4028811b671beae801671bfa71a0025e', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-05 16:57:13', NULL, 1, 0, '4028811b671beae801671bfa71a0025e', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);



ALTER TABLE uk_callcenter_pbxhost ADD conference tinyint DEFAULT 0 COMMENT '是否启用会议';
ALTER TABLE uk_callcenter_pbxhost ADD initleave tinyint DEFAULT 0 COMMENT '创建人挂断是否结束会议';

ALTER TABLE uk_callcenter_pbxhost ADD conferencenum VARCHAR(50) DEFAULT NULL COMMENT '会议默认接入号码';

ALTER TABLE uk_callcenter_pbxhost ADD autoready tinyint(4) DEFAULT 0 COMMENT '挂断后自动转就绪';
ALTER TABLE uk_callcenter_pbxhost ADD afterstatus varchar(20) DEFAULT NULL COMMENT '挂断状态';
ALTER TABLE uk_callcenter_pbxhost ADD orgtranstype varchar(200) DEFAULT NULL COMMENT '需要转接的呼叫类型';


ALTER TABLE uk_callcenter_router ADD enablemoh tinyint DEFAULT 0 COMMENT '启用保持提示音';
ALTER TABLE uk_callcenter_router ADD moh varchar(200) DEFAULT NULL COMMENT '保持音乐';

ALTER TABLE uk_callcenter_router ADD enabletransmusic tinyint DEFAULT 0 COMMENT '启用转接提示音';
ALTER TABLE uk_callcenter_router ADD transmusic varchar(200) DEFAULT NULL COMMENT '转接提示音';

ALTER TABLE uk_callcenter_router ADD enabletransmusic tinyint DEFAULT 0 COMMENT '启用转接提示音';
ALTER TABLE uk_callcenter_router ADD enableworktime tinyint DEFAULT 0 COMMENT '启用工作时间控制';

ALTER TABLE uk_callcenter_router ADD mday varchar(200) DEFAULT NULL COMMENT '工作日期';
ALTER TABLE uk_callcenter_router ADD wday varchar(200) DEFAULT NULL COMMENT '工作周';
ALTER TABLE uk_callcenter_router ADD timeofday varchar(200) DEFAULT NULL COMMENT '工作时间范围';
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e96e4a5fc0016e4a95d5fa00ed', '我的通话', 'pub', 'A10_A01_A07', NULL, 'auth', '402881ef612b1f5b01612cee4fbb058a', NULL, NULL, '&#x756e646566696e6564;', NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-08 18:36:03', NULL, 0, 0, '402888815d2fe37f015d2fe75cc80002', 0, 0, '/apps/callcenter/service/owner/index.html', 'webim', '1', NULL, 'left', 0);

ALTER TABLE uk_organ ADD monitor tinyint DEFAULT 0 COMMENT '允许监控其他坐席状态';
ALTER TABLE uk_systemconfig ADD language VARCHAR(50) DEFAULT 'zh_CN' COMMENT '系统语言（默认中文）';
ALTER TABLE uk_systemconfig ADD xunfeittsvoicename VARCHAR(50) DEFAULT NULL COMMENT 'tts-VoiceName 讯飞在线语音合成-发音人参数';

ALTER TABLE uk_que_survey_question ADD enabletransfercon tinyint(4) DEFAULT '0' COMMENT '该话术节点，开启逻辑转人工';
ALTER TABLE uk_que_survey_question ADD transferconditions VARCHAR(50) DEFAULT NULL COMMENT '逻辑条件（字典维护）';
ALTER TABLE uk_que_survey_question ADD transfervalue text COMMENT '逻辑条件值';
ALTER TABLE uk_que_survey_question ADD transferactivity VARCHAR(50) DEFAULT NULL COMMENT '活动ID，逻辑转人工的人工坐席范围，通过选择指定电销活动确定，转接到指定活动下的空闲坐席';
ALTER TABLE uk_que_survey_question ADD transferwvtype VARCHAR(50) COMMENT '转人工提示语类型';
ALTER TABLE uk_que_survey_question ADD transfervoice VARCHAR(50) COMMENT '转人工提示语音';
ALTER TABLE uk_que_survey_question ADD transferword text COMMENT '转人工提示语文字';

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6e6d0b05016e6d0e1eae000f', '活动下空闲坐席大于条件值', 'pub', 'agents', 'ukewo', 'layui-icon', '402880eb6e6d0b05016e6d0c56b9000a', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-15 11:14:32', NULL, 1, 0, '402880eb6e6d0b05016e6d0c56b9000a', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6e6d0b05016e6d0c56b9000a', '话术节点转人工逻辑条件', 'pub', 'com.dic.salepatter.transferconditions', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-15 11:12:35', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);


ALTER TABLE uk_cdr_both MODIFY COLUMN uduration bigint;


ALTER TABLE uk_callcenter_pbxhost ADD loginready tinyint(4) DEFAULT 0 COMMENT '登录转就绪';
ALTER TABLE uk_callcenter_pbxhost ADD logoutready tinyint(4) DEFAULT 0 COMMENT '登出转就绪';

ALTER TABLE uk_callcenter_event ADD aitrans tinyint(4) DEFAULT 0 COMMENT '机器人转接';
ALTER TABLE uk_callcenter_event ADD aiext varchar(50) DEFAULT NULL COMMENT '机器人号码';

ALTER TABLE uk_act_callnames ADD aitrans tinyint(4) DEFAULT 0 COMMENT '机器人转接';
ALTER TABLE uk_act_callnames ADD aiext varchar(50) DEFAULT NULL COMMENT '机器人号码';

ALTER TABLE uk_act_callnames_his ADD aitrans tinyint(4) DEFAULT 0 COMMENT '机器人转接';
ALTER TABLE uk_act_callnames_his ADD aiext varchar(50) DEFAULT NULL COMMENT '机器人号码';


ALTER TABLE uk_act_callnames ADD aitransqus varchar(50) DEFAULT NULL COMMENT '转接问题ID';
ALTER TABLE uk_act_callnames ADD aitranstime datetime DEFAULT NULL COMMENT '机器人转接时间';
ALTER TABLE uk_act_callnames ADD aitransduration int DEFAULT 0 COMMENT '机器人转接是通话时长';

ALTER TABLE uk_act_callnames ADD membersessionid VARCHAR(50) DEFAULT NULL COMMENT '转接前ID';

ALTER TABLE uk_callcenter_event ADD aitransqus varchar(50) DEFAULT NULL COMMENT '转接问题ID';
ALTER TABLE uk_callcenter_event ADD aitranstime datetime DEFAULT NULL COMMENT '机器人转接时间';
ALTER TABLE uk_callcenter_event ADD aitransduration int DEFAULT 0 COMMENT '机器人转接是通话时长';

ALTER TABLE uk_systemconfig ADD xunfeittsapisecret text COMMENT '讯飞tts-secret';


ALTER TABLE uk_callcenter_router ADD exportvars tinyint DEFAULT 0 COMMENT '启用复制参数';
ALTER TABLE uk_callcenter_router ADD sysvars tinyint DEFAULT 0 COMMENT '启用系统参数';
ALTER TABLE uk_callcenter_router ADD billing tinyint DEFAULT 0 COMMENT '启用计费';

ALTER TABLE uk_callcenter_router ADD billingheat int DEFAULT 0 COMMENT '计费周期';
ALTER TABLE uk_callcenter_router ADD billingaccount VARCHAR(50) DEFAULT 0 COMMENT '计费账号';
ALTER TABLE uk_callcenter_router ADD billingrate VARCHAR(255) DEFAULT NULL COMMENT '计费费率';

ALTER TABLE uk_callcenter_pbxhost ADD billing tinyint(4) DEFAULT 0 COMMENT '强制开启计费';

ALTER TABLE uk_spt_media ADD bitrate varchar(20) DEFAULT NULL COMMENT '码流';
ALTER TABLE uk_spt_media ADD samplingrate varchar(20) DEFAULT NULL COMMENT '采样频率';
ALTER TABLE uk_spt_media ADD channelmode varchar(20) DEFAULT NULL COMMENT '是否立体声';
ALTER TABLE uk_spt_media ADD tracklength varchar(20) DEFAULT NULL COMMENT '时长';
ALTER TABLE uk_spt_media ADD tracklengthtime int DEFAULT 0 COMMENT '时长';
ALTER TABLE uk_spt_media ADD channels int DEFAULT 0 COMMENT '声道数量';


ALTER TABLE uk_callcenter_event MODIFY COLUMN RECORDFILENAME varchar(255) DEFAULT NULL COMMENT '录音文件名';

ALTER TABLE uk_que_survey_question ADD transnode VARCHAR(50) COMMENT '转话术节点';

ALTER TABLE uk_que_survey_question ADD nmline tinyint(4) DEFAULT '0' COMMENT '该话术节点，是否非主线节点';



ALTER TABLE uk_callcenter_event ADD ai tinyint(4) DEFAULT 0 COMMENT '是否机器人会话';
ALTER TABLE uk_callcenter_event ADD aieventid varchar(50) DEFAULT NULL COMMENT '转接前通话记录ID';
ALTER TABLE uk_callcenter_event ADD asrtimes int(11) DEFAULT 0 COMMENT 'asr次数';
ALTER TABLE uk_callcenter_event ADD ttstimes int(11) DEFAULT 0 COMMENT 'tts次数';
ALTER TABLE uk_callcenter_event ADD timeouttimes int(11) DEFAULT 0 COMMENT '当前节点超时次数';
ALTER TABLE uk_callcenter_event ADD errortimes int(11) DEFAULT 0 COMMENT '当前节点错误次数';
ALTER TABLE uk_callcenter_event ADD nmlinetimes int(11) DEFAULT 0 COMMENT '非主线节点次数';


ALTER TABLE uk_act_callnames ADD ai tinyint(4) DEFAULT 0 COMMENT '是否机器人会话';
ALTER TABLE uk_act_callnames ADD aieventid varchar(50) DEFAULT NULL COMMENT '转接前通话记录ID';
ALTER TABLE uk_act_callnames ADD asrtimes int(11) DEFAULT 0 COMMENT 'asr次数';
ALTER TABLE uk_act_callnames ADD ttstimes int(11) DEFAULT 0 COMMENT 'tts次数';
ALTER TABLE uk_act_callnames ADD timeouttimes int(11) DEFAULT 0 COMMENT '当前节点超时次数';
ALTER TABLE uk_act_callnames ADD errortimes int(11) DEFAULT 0 COMMENT '当前节点错误次数';
ALTER TABLE uk_act_callnames ADD nmlinetimes int(11) DEFAULT 0 COMMENT '非主线节点次数';


ALTER TABLE uk_que_survey_question ADD nmlinerepeat varchar(50) DEFAULT NULL COMMENT '非主线-最大重复次数';
ALTER TABLE uk_que_survey_question ADD nmlineoperate varchar(50) DEFAULT NULL COMMENT '非主线错误语-到达最大次数的操作';
ALTER TABLE uk_que_survey_question ADD nmlinetrans varchar(50) DEFAULT NULL COMMENT '非主线错误语-转接号码';
ALTER TABLE uk_que_survey_question ADD nmlinetypeup varchar(50) DEFAULT NULL COMMENT '非主线错误语-挂断提示语类型';
ALTER TABLE uk_que_survey_question ADD nmlinewordup varchar(50) DEFAULT NULL COMMENT '非主线误语-挂断提示语（文字）';
ALTER TABLE uk_que_survey_question ADD nmlinevoiceup varchar(50) DEFAULT NULL COMMENT '非主线错误语-挂断提示语（语音ID）';

ALTER TABLE uk_que_survey_question ADD nmlinetypematch varchar(50) DEFAULT NULL COMMENT '非主线错误语-类型';
ALTER TABLE uk_que_survey_question ADD nmlinetype int(11) DEFAULT 0 COMMENT '非主线类型';

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402881e66ea28c8b016ea2904fd7003c', '坐席满意度报表', 'pub', 'A10_A03_A04', NULL, 'auth', '402882676793db4a016793f098d4004f', NULL, NULL, '&#x756e646566696e6564;', NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-25 20:36:36', NULL, 0, 0, '402888815d2fe37f015d2fe75cc80002', 0, 0, '/service/stats/callcenter/satisf.html', 'webim', '1', NULL, 'left', 0);


CREATE TABLE `uk_callcenter_event_sip` (
  `ID` varchar(100) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `tracesip` text COMMENT 'SIP消息记录',
  `answersip` text COMMENT '应答事件SIP',
  `hangupsip` text COMMENT '挂断事件SIP',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='通话记录SIP表';

ALTER TABLE uk_callcenter_event MODIFY COLUMN accountdes VARCHAR(255) COMMENT '客户账号信息';

ALTER TABLE uk_callcenter_event DROP tracesip;
ALTER TABLE uk_callcenter_event DROP answersip;
ALTER TABLE uk_callcenter_event DROP hangupsip;


ALTER TABLE uk_que_survey_question ADD qusbussop tinyint(4) DEFAULT 0 COMMENT '启用触发业务';
ALTER TABLE uk_que_survey_question ADD qusbusslist varchar(100) DEFAULT NULL COMMENT '业务类型';

ALTER TABLE uk_que_survey_answer ADD bussop tinyint(4) DEFAULT 0 COMMENT '启用触发业务';
ALTER TABLE uk_que_survey_answer ADD busslist varchar(100) DEFAULT NULL COMMENT '业务类型';

ALTER TABLE uk_contacts ADD extinfo text DEFAULT NULL COMMENT '扩展业务字段';
ALTER TABLE uk_act_config ADD autoready tinyint(4) DEFAULT 0 COMMENT '自动进入名单等待';

ALTER TABLE uk_act_callnames ADD privatefield tinyint(4) DEFAULT 0 COMMENT '是否启用敏感字段隐藏';

ALTER TABLE uk_act_callnames ADD priphonenumber VARCHAR(50) DEFAULT NULL COMMENT '敏感字段隐藏实际值';



CREATE TABLE `uk_report_measure` (
  `ID` varchar(100) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `MEASUREDATA` int(11) DEFAULT 0 COMMENT '指标',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='通话记录SIP表';


ALTER TABLE uk_callcenter_pbxhost ADD previewring varchar(100) DEFAULT NULL COMMENT '预览铃声';

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6e8820fe016e8826dcec0017', '中文繁體', 'pub', 'zh_TC', 'ukewo', 'layui-icon', '402880eb6e87feb7016e88118fe60023', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-20 17:31:18', NULL, 1, 1, '402880eb6e87feb7016e88118fe60023', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6e87feb7016e88121676002c', 'English', 'pub', 'en_US', 'ukewo', 'layui-icon', '402880eb6e87feb7016e88118fe60023', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-20 17:08:37', NULL, 1, 2, '402880eb6e87feb7016e88118fe60023', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6e87feb7016e8811ded60028', '中文简体', 'pub', 'zh_CN', 'ukewo', 'layui-icon', '402880eb6e87feb7016e88118fe60023', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-20 17:08:22', NULL, 1, 0, '402880eb6e87feb7016e88118fe60023', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6e87feb7016e88118fe60023', '国际化多语言切换', 'pub', 'com.dic.international.language', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-11-20 17:08:02', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);


ALTER TABLE uk_callcenter_event ADD waittime INT(11) DEFAULT 0 COMMENT '等待时长';
ALTER TABLE uk_callcenter_event ADD transfaild tinyint(4) DEFAULT 0 COMMENT '人工转接失败';


ALTER TABLE uk_jobdetail MODIFY COLUMN aithreads int(11) COMMENT '机器人线程数量';

ALTER TABLE uk_callcenter_event ADD priphone varchar(50) DEFAULT NULL COMMENT '隐私号码';


ALTER TABLE uk_callcenter_router ADD routerdata varchar(50) DEFAULT NULL COMMENT '规则信息';


ALTER TABLE uk_spt_salespatter ADD enabletransfercon tinyint(4) DEFAULT '0' COMMENT '该话术节点，开启逻辑转人工';
ALTER TABLE uk_spt_salespatter ADD transferconditions VARCHAR(50) DEFAULT NULL COMMENT '逻辑条件（字典维护）';
ALTER TABLE uk_spt_salespatter ADD transfervalue text COMMENT '逻辑条件值';
ALTER TABLE uk_spt_salespatter ADD transferactivity VARCHAR(50) DEFAULT NULL COMMENT '活动ID，逻辑转人工的人工坐席范围，通过选择指定电销活动确定，转接到指定活动下的空闲坐席';
ALTER TABLE uk_spt_salespatter ADD transferwvtype VARCHAR(50) COMMENT '转人工提示语类型';
ALTER TABLE uk_spt_salespatter ADD transfervoice VARCHAR(50) COMMENT '转人工提示语音';
ALTER TABLE uk_spt_salespatter ADD transferword text COMMENT '转人工提示语文字';
ALTER TABLE uk_spt_salespatter ADD transferequal VARCHAR(50) DEFAULT NULL COMMENT '逻辑条件比较方式';
ALTER TABLE uk_spt_salespatter ADD transnode VARCHAR(50) DEFAULT NULL  COMMENT '转话术节点';

ALTER TABLE uk_spt_salespatter ADD quetype int(11) DEFAULT 0 COMMENT '逻辑类型';
ALTER TABLE uk_spt_salespatter ADD trans VARCHAR(50) DEFAULT NULL  COMMENT '转接号码';


ALTER TABLE uk_que_survey_question ADD transferequal VARCHAR(50) DEFAULT NULL COMMENT '逻辑条件比较方式';



ALTER TABLE uk_act_callnames ADD transcon tinyint(4) DEFAULT 0 COMMENT '是否条件转';
ALTER TABLE uk_act_callnames ADD transconid varchar(50) DEFAULT NULL COMMENT '转接条件';

ALTER TABLE uk_callcenter_event ADD transcon tinyint(4) DEFAULT 0 COMMENT '是否条件转';
ALTER TABLE uk_callcenter_event ADD transconid varchar(50) DEFAULT NULL COMMENT '转接条件';

ALTER TABLE uk_systemconfig ADD voiceaddr varchar(200) DEFAULT NULL COMMENT '语音播放的访问地址';
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402881e76f239d5c016f239ec3a6000a', '通话记录与名单信息导出附加列名', 'pub', 'com.dic.callcenter.names.export', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-20 22:03:24', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);


CREATE TABLE `uk_blacklist_intercept` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `phone` varchar(50) DEFAULT NULL COMMENT '显示手机号',
  `priphone` varchar(50) DEFAULT NULL COMMENT '原始手机号',
  `channel` varchar(50) DEFAULT NULL COMMENT '拦截渠道(webim 在线客服 | callcenter 呼叫中心)',
	`type` varchar(50) DEFAULT NULL COMMENT '拦截类型(webim 在线PC客服接入| weixin 微信接入| 预测式外呼 forecastsales | 电销Ai calloutai)',
	`userid` varchar(50) DEFAULT NULL COMMENT '',
	`sessionid` varchar(50) DEFAULT NULL COMMENT '',
	`agentuser` varchar(50) DEFAULT NULL COMMENT '',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='黑名单拦截记录表';

INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f455a645364af', '共和市', 'pub', '633000', 'ukewo', 'layui-icon', '297e1e874f83129d014f8321565d00df', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 11:15:49', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4559e6f463ff', '海晏市', 'pub', '632900', 'ukewo', 'layui-icon', '297e1e874f83129d014f8321565d00df', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 11:15:17', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f455925ed62da', '商州市', 'pub', '611100', 'ukewo', 'layui-icon', '297e1e874f83129d014f8321564100dd', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 11:14:27', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4554405f5b5e', '神农架市', 'pub', '429060', 'ukewo', 'layui-icon', '297e1e874f83129d014f832155bf00d3', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 11:09:07', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4550d4915665', '集宁市', 'pub', '153200', 'ukewo', 'layui-icon', '297e1e874f83129d014f8321550a00c7', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 11:05:22', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f454f100c5400', '临河市', 'pub', '153100', 'ukewo', 'layui-icon', '297e1e874f83129d014f8321550a00c7', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 11:03:26', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f454d4da85131', '海拉尔市', 'pub', '153000', 'ukewo', 'layui-icon', '297e1e874f83129d014f8321550a00c7', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 11:01:31', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f454b82ba4e6f', '普洱市', 'pub', '533500', 'ukewo', 'layui-icon', '297e1e874f83129d014f8321562400db', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:59:34', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6e7b06d2', '沙嘴街道', 'pub', '429053', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 1, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6e8406d3', '郑场镇', 'pub', '429054', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 2, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6e8b06d4', '毛嘴镇', 'pub', '429055', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 3, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6e9206d5', '剅河镇', 'pub', '429056', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 4, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6e9a06d6', '三伏潭镇', 'pub', '429057', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 5, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6ea106d8', '胡场镇', 'pub', '429058', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 6, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6ea906d9', '长埫口镇', 'pub', '429059', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 7, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6eb106da', '西流河镇', 'pub', '429060', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 8, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6eb806db', '彭场镇', 'pub', '429061', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 9, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6ec006dc', '沙湖镇', 'pub', '429062', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 10, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6ec706dd', '杨林尾镇', 'pub', '429063', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 11, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6ed306de', '张沟镇', 'pub', '429064', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 12, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6eed06df', '郭河镇', 'pub', '429065', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 13, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6ef406e0', '沔城回族镇', 'pub', '429066', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 14, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6efa06e1', '通海口镇', 'pub', '429067', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 15, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451b6f0106e2', '陈场镇', 'pub', '429068', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:07:03', '2019-12-27 10:07:03', 0, 16, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451846b702a3', '老河口市', 'pub', '429039', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:03:36', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f45180a280252', '宜城市', 'pub', '429038', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:03:21', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4517d105020d', '谷城县', 'pub', '429037', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:03:06', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451783a901aa', '保康县', 'pub', '429036', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:02:46', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451765140186', '干河街道', 'pub', '429051', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:02:38', '2019-12-27 10:02:38', 0, 1, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4517651b0187', '龙华山街道', 'pub', '429052', 'ukewo', NULL, '2c92a1e36f427d60016f450f110076b6', NULL, NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:02:38', '2019-12-27 10:02:38', 0, 2, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f45174a670164', '南漳县', 'pub', '429035', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:02:31', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4516faef00f8', '枣阳市', 'pub', '429034', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:02:11', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4516b0de008f', '襄州区', 'pub', '429033', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:01:52', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f451685880050', '樊城区', 'pub', '429032', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:01:41', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f4516058f7f9d', '襄城区', 'pub', '429031', 'ukewo', 'layui-icon', '2c92a1e36f427d60016f450680ff6692', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 10:01:08', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f450f110076b6', '仙桃市', 'pub', '429050', 'ukewo', 'layui-icon', '297e1e874f83129d014f832155bf00d3', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 09:53:32', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `uckefu`.`uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c92a1e36f427d60016f450680ff6692', '襄阳市', 'pub', '429030', 'ukewo', 'layui-icon', '297e1e874f83129d014f832155bf00d3', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-12-27 09:44:11', NULL, 1, 0, '297e1e874f83129d014f832090e200b8', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);

ALTER TABLE uk_callcenter_event ADD con_quality varchar(50) DEFAULT NULL ;
ALTER TABLE uk_callcenter_event ADD con_qualitypass varchar(50) DEFAULT NULL ;
ALTER TABLE uk_callcenter_event ADD con_invitation varchar(50) DEFAULT NULL ;
ALTER TABLE uk_callcenter_event ADD con_qualitysubmit varchar(50) DEFAULT NULL ;

ALTER TABLE uk_jobdetail ADD skill VARCHAR(50) DEFAULT NULL COMMENT '语音平台-技能分组';

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6f83e2a3016f83fa43d30032', '话务主叫统计报表', 'pub', 'A10_A03_A05', NULL, 'auth', '402882676793db4a016793f098d4004f', NULL, NULL, '&#x756e646566696e6564;', NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-08 15:06:54', NULL, 0, 0, '402888815d2fe37f015d2fe75cc80002', 0, 0, '/service/stats/callcenter/discaller.html', 'webim', '1', NULL, 'left', 0);


ALTER TABLE uk_servicesummary ADD complaints TINYINT(4) DEFAULT 0 COMMENT '是否投诉';
ALTER TABLE uk_servicesummary ADD businesstype text COMMENT '业务类型（字典项选择）';
ALTER TABLE uk_servicesummary ADD province VARCHAR(50) DEFAULT NULL COMMENT '省';
ALTER TABLE uk_servicesummary ADD city VARCHAR(50) DEFAULT NULL COMMENT '市';
ALTER TABLE uk_servicesummary ADD area VARCHAR(50) DEFAULT NULL COMMENT '县';
ALTER TABLE uk_servicesummary ADD cusname VARCHAR(255) DEFAULT NULL COMMENT '客户名称';
ALTER TABLE uk_servicesummary ADD dispatchtime datetime DEFAULT NULL COMMENT '派单时间';
ALTER TABLE uk_servicesummary ADD ansorgan VARCHAR(50) DEFAULT NULL COMMENT '回复部门ID';
ALTER TABLE uk_servicesummary ADD ansagent VARCHAR(50) DEFAULT NULL COMMENT '回复人ID';
ALTER TABLE uk_servicesummary ADD anstime datetime DEFAULT NULL COMMENT '回复时间';
ALTER TABLE uk_servicesummary ADD finish TINYINT(4) DEFAULT 0 COMMENT '是否已完结';
ALTER TABLE uk_servicesummary ADD anscontent text COMMENT '回复内容';

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6f89717e016f897a21c80024', '已处理呼叫中心-导出', 'pub', 'A08_A03_A04_A01', NULL, 'auth', '402881ef612b1f5b01612cdce2a4056f', NULL, NULL, '<i class=\"kfont\">&#xe672;</i>', NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-09 16:44:40', NULL, 0, 0, '402888815d2fe37f015d2fe75cc80002', 0, 0, 'javascript:void(0)', 'webim', '1', NULL, 'left', 0);
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6f89717e016f8979ae45001f', '未处理呼叫中心-导出', 'pub', 'A08_A03_A03_A01', NULL, 'auth', '402881ef612b1f5b01612cdce2a4056f', NULL, NULL, '<i class=\"kfont\">&#xe672;</i>', NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-09 16:44:10', NULL, 0, 0, '402888815d2fe37f015d2fe75cc80002', 0, 0, 'javascript:void(0)', 'webim', '1', NULL, 'left', 0);
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6f89717e016f8978f41e001a', '呼叫中心-导出', 'pub', 'A08_A03_A05_A01', NULL, 'auth', '402881ef612b1f5b01612cdce2a4056f', NULL, NULL, '<i class=\"kfont\">&#xe672;</i>', NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-09 16:43:23', NULL, 0, 0, '402888815d2fe37f015d2fe75cc80002', 0, 0, 'javascript:void(0)', 'webim', '1', NULL, 'left', 0);
INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6f8961bd016f896b3c43002a', '呼叫中心', 'pub', 'A08_A03_A05', NULL, 'auth', '402881ef612b1f5b01612cdce2a4056f', NULL, NULL, '&#x756e646566696e6564;', NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-09 16:28:24', NULL, 0, 0, '402888815d2fe37f015d2fe75cc80002', 0, 0, '/apps/callcenter/summary/data.html', 'webim', '1', NULL, 'left', 0);
UPDATE `uk_sysdic` SET `NAME` = '未处理呼叫中心', `TITLE` = 'pub', `CODE` = 'A08_A03_A03', `ORGI` = 'ukewo', `CTYPE` = 'layui-icon', `PARENTID` = '402881ef612b1f5b01612cdce2a4056f', `DESCRIPTION` = '', `MEMO` = NULL, `ICONSTR` = '&#x756e646566696e6564;', `ICONSKIN` = '', `CATETYPE` = 'webim', `CREATER` = '297e8c7b455798280145579c73e501c1', `CREATETIME` = '2018-12-13 09:03:59', `UPDATETIME` = NULL, `HASCHILD` = 0, `SORTINDEX` = 0, `DICID` = '402888815d2fe37f015d2fe75cc80002', `DEFAULTVALUE` = 0, `DISCODE` = 0, `URL` = '/apps/callcenter/summary/index.html', `MODULE` = 'webim', `MLEVEL` = '3', `RULES` = NULL, `MENUTYPE` = 'left', `tenant` = 0 WHERE `ID` = '402881ef612b1f5b01612ce85460057c';


ALTER TABLE uk_callcenter_extention ADD eoseng int(11) DEFAULT 0 COMMENT '用户说完之后没说话的静音阈值,一般400, >0有效';
ALTER TABLE uk_callcenter_extention ADD eostime int(11) DEFAULT 0 COMMENT '用户说话停顿超时，说话结束,单位ms,越小越容易结束一般500, >0有效';
ALTER TABLE uk_callcenter_extention ADD soseng int(11) DEFAULT 0 COMMENT '用户开始说话的声音阀值,越小越敏感,一般800,  >0有效';

ALTER TABLE uk_callcenter_extention ADD assigned tinyint(4) DEFAULT 0 COMMENT '是否分配 多租户使用';




ALTER TABLE uk_callcenter_skillext ADD budeltime int(11) DEFAULT 0 COMMENT '忙延迟时间';
ALTER TABLE uk_callcenter_skillext ADD level int(11) DEFAULT 0 COMMENT '级别';
ALTER TABLE uk_callcenter_skillext ADD position int(11) DEFAULT 0 COMMENT '地位';
ALTER TABLE uk_callcenter_skillext ADD calltimeout int(11) DEFAULT 0 COMMENT '呼叫等待超时时长';

ALTER TABLE uk_callcenter_skillext ADD maxnoanswer int(11) DEFAULT 0 COMMENT '最大无应答';
ALTER TABLE uk_callcenter_skillext ADD wrapuptime int(11) DEFAULT 0 COMMENT '话后处理时间';
ALTER TABLE uk_callcenter_skillext ADD redeltime int(11) DEFAULT 0 COMMENT '客户端拒绝后延迟时间';

ALTER TABLE uk_callcenter_skillext ADD status VARCHAR(50) DEFAULT NULL COMMENT '状态';


ALTER TABLE uk_callcenter_skill ADD abanresumealo VARCHAR(50) DEFAULT NULL COMMENT '重新进入队列 true/false';
ALTER TABLE uk_callcenter_skill ADD disabanafter int(11) DEFAULT 0 COMMENT '最大放弃时长';
ALTER TABLE uk_callcenter_skill ADD announcefrequency int(11) DEFAULT 0 COMMENT '播报频率';
ALTER TABLE uk_callcenter_skill ADD announcesound VARCHAR(255) DEFAULT NULL COMMENT '播报语音';

ALTER TABLE uk_callcenter_skill ADD maxwaittime int(11) DEFAULT 0 COMMENT '最大等待时间';
ALTER TABLE uk_callcenter_skill ADD maxwtnoagent int(11) DEFAULT 0 COMMENT '最大等待时间与无代理';
ALTER TABLE uk_callcenter_skill ADD maxwtnareached int(11) DEFAULT 0 COMMENT '拒绝新来电间隔时间';
ALTER TABLE uk_callcenter_skill ADD trapply VARCHAR(50) DEFAULT NULL COMMENT '等级规则应用 true/false';
ALTER TABLE uk_callcenter_skill ADD trwaitsecond int(11) DEFAULT 0 COMMENT '等级规则设置的时间';

ALTER TABLE uk_callcenter_skill ADD mohsound VARCHAR(255) DEFAULT NULL COMMENT '语音文件参数';
ALTER TABLE uk_callcenter_skill ADD recordtemplate VARCHAR(255) DEFAULT NULL COMMENT '录音文件参数';
ALTER TABLE uk_callcenter_skill ADD timebasescore VARCHAR(255) DEFAULT NULL COMMENT '时基分数';

ALTER TABLE uk_callcenter_skill ADD strategy VARCHAR(255) DEFAULT NULL COMMENT '策略模式';

ALTER TABLE uk_callcenter_skill ADD trmultiplylevel VARCHAR(255) DEFAULT NULL COMMENT '等级规则级别 true/false';
ALTER TABLE uk_callcenter_skill ADD trnoagentnowait VARCHAR(255) DEFAULT NULL COMMENT '是否跳过等级规则 true/false';

ALTER TABLE uk_tagrelation ADD orgi VARCHAR(50) DEFAULT NULL ;

ALTER TABLE uk_callcenter_siptrunk ADD dynamic TINYINT(4) DEFAULT 0 COMMENT '启用动态路由的功能，启用后，允许根据被叫归属地选择出局线路';

CREATE TABLE `uk_callcenter_siptrunk_dynamic` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `siptrunkid` varchar(32) DEFAULT NULL COMMENT 'SIP网关ID',
  `hostid` varchar(32) DEFAULT NULL COMMENT '语音平台服务器ID',
  `province` varchar(50) DEFAULT NULL COMMENT '省',
  `city` varchar(50) DEFAULT NULL COMMENT '市',
  `area` varchar(50) DEFAULT NULL COMMENT '县',
  `gatewayid` varchar(50) DEFAULT NULL COMMENT '归属地指定绑定的网关',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='动态路由配置表';

ALTER TABLE uk_servicesummary ADD processstatus VARCHAR(50) DEFAULT NULL COMMENT '业务状态（字典项com.dic.rx.processstatus）';
ALTER TABLE uk_servicesummary ADD conductor VARCHAR(50) DEFAULT NULL COMMENT '处理人（业务）';
ALTER TABLE uk_servicesummary ADD conductoruname VARCHAR(255) DEFAULT NULL COMMENT '处理人展示名（用户账号的uname）';


INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6fa72d5b016fa736eaba0022', '瑞祥定制服务小结-处理状态', 'pub', 'com.dic.rx.processstatus', NULL, 'data', '0', '瑞祥定制服务小结-处理状态(下拉框)', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-15 11:19:51', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0);

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6fa72d5b016fa7377c71002f', '已反馈', 'pub', '3', 'ukewo', 'layui-icon', '402880eb6fa72d5b016fa736eaba0022', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-15 11:20:29', NULL, 1, 0, '402880eb6fa72d5b016fa736eaba0022', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6fa72d5b016fa7375849002b', '派单', 'pub', '2', 'ukewo', 'layui-icon', '402880eb6fa72d5b016fa736eaba0022', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-15 11:20:19', NULL, 1, 0, '402880eb6fa72d5b016fa736eaba0022', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);

INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880eb6fa72d5b016fa737334e0027', '已解决', 'pub', '1', 'ukewo', 'layui-icon', '402880eb6fa72d5b016fa736eaba0022', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-01-15 11:20:10', NULL, 1, 0, '402880eb6fa72d5b016fa736eaba0022', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);



ALTER TABLE uk_callcenter_pbxhost ADD `bussop` tinyint(4) DEFAULT '0' COMMENT '触发业务操作';
ALTER TABLE uk_callcenter_pbxhost ADD `busslist` text COMMENT '触发业务列表';
ALTER TABLE uk_callcenter_pbxhost ADD `bussexeclist` text COMMENT '执行业务列表';

ALTER TABLE uk_callcenter_pbxhost ADD `igr` tinyint(4) DEFAULT '0' COMMENT '性别年龄识别 - 开关';
ALTER TABLE uk_callcenter_pbxhost ADD `igrappid` varchar(50) DEFAULT NULL COMMENT '性别年龄识别 - APPID';
ALTER TABLE uk_callcenter_pbxhost ADD `igrapisec` varchar(50) DEFAULT NULL COMMENT '性别年龄识别 - APISecret';
ALTER TABLE uk_callcenter_pbxhost ADD `igrapikey` varchar(50) DEFAULT NULL COMMENT '性别年龄识别 - APIKey';
ALTER TABLE uk_callcenter_pbxhost ADD `igrhost` varchar(255) DEFAULT NULL COMMENT '性别年龄识别 - 接口地址';
ALTER TABLE uk_callcenter_pbxhost ADD `igrtype` varchar(50) DEFAULT NULL COMMENT '性别年龄识别引擎（讯飞：xfigr）';

ALTER TABLE uk_callcenter_event ADD `igr` tinyint(4) DEFAULT '0' COMMENT '是否进行性别年龄识别';
ALTER TABLE uk_callcenter_event ADD `igrage`  varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别的年龄[0：middle(12~40岁) 1：child（0~12岁）2：old（40岁以上）]';
ALTER TABLE uk_callcenter_event ADD `igrchild`  varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为儿童的概率值，儿童、中年、老年概率值最大的为最终结果	';
ALTER TABLE uk_callcenter_event ADD `igrmiddle`  varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为中年的概率值，儿童、中年、老年概率值最大的为最终结果	';
ALTER TABLE uk_callcenter_event ADD `igrold`  varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为老年的概率值，儿童、中年、老年概率值最大的为最终结果	';

ALTER TABLE uk_callcenter_event ADD `igrgender`  varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别的性别 [0：女性 1：男性]';
ALTER TABLE uk_callcenter_event ADD `igrfemale`  varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为女声的概率值，女声、男声概率值较大的为最终结果';
ALTER TABLE uk_callcenter_event ADD `igrmale`  varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为男声的概率值，女声、男声概率值较大的为最终结';

ALTER TABLE uk_callcenter_event ADD `igrbegin` datetime DEFAULT NULL COMMENT '开始识别时间';
ALTER TABLE uk_callcenter_event ADD `igrend` datetime DEFAULT NULL COMMENT '结束识别时间';

ALTER TABLE uk_xiaoe_topic ADD bussclist VARCHAR(255) DEFAULT NULL COMMENT '触发的业务子级操作列表';
ALTER TABLE uk_xiaoe_scene ADD bussclist VARCHAR(255) DEFAULT NULL COMMENT '触发的业务子级操作列表';

ALTER TABLE uk_que_survey_question ADD enabledenoise tinyint(4) DEFAULT 0 COMMENT '话术开启降噪';
ALTER TABLE uk_que_survey_question ADD denoise int(11) DEFAULT 0 COMMENT '降噪强度';

ALTER TABLE uk_callcenter_extention ADD denoise int(11) DEFAULT 0 COMMENT '降噪强度';

ALTER TABLE uk_callcenter_event ADD aicollect tinyint(4) DEFAULT 0 COMMENT '标记该通话，是否是全流程机器人采集数据';

ALTER TABLE uk_que_survey_answer ADD defaultans tinyint(4) DEFAULT 0 COMMENT '是否默认匹配答案';


ALTER TABLE uk_callcenter_event ADD `igrvoicetime` int(11) DEFAULT 0 COMMENT '性别年龄识别 - 语音时长';
ALTER TABLE uk_callcenter_event ADD `igrstatus` varchar(50) DEFAULT NULL COMMENT '性别年龄识别 - 当前状态';
ALTER TABLE uk_callcenter_event ADD `igrvoice` varchar(255) DEFAULT NULL COMMENT '性别年龄识别 - 语音文件';

ALTER TABLE uk_callcenter_event ADD con_surnames VARCHAR(255) DEFAULT NULL COMMENT '联系人姓名';
ALTER TABLE uk_callcenter_event ADD con_intention VARCHAR(50) DEFAULT NULL COMMENT '意向度';

ALTER TABLE uk_callcenter_extention ADD desktopmonitor tinyint(4) DEFAULT 0 COMMENT '启用桌面监控';
ALTER TABLE uk_callcenter_extention ADD videomonitor tinyint(4) DEFAULT 0 COMMENT '启用视频监控';

ALTER TABLE uk_jobdetail ADD batid varchar(50) DEFAULT NULL COMMENT '批次ID';

ALTER TABLE uk_systemconfig ADD snippeturl VARCHAR(255) DEFAULT NULL COMMENT '上传录音碎片地址';
ALTER TABLE uk_systemconfig ADD recordurl VARCHAR(255) DEFAULT NULL COMMENT '上传录音地址';

ALTER TABLE uk_tag ADD sortindex int(11) DEFAULT 0 COMMENT '排序';

CREATE TABLE `uk_salespatter_multianswer` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `processid` varchar(50) DEFAULT NULL COMMENT '所属问卷ID',
  `answerid` varchar(50) DEFAULT NULL COMMENT '所属答案ID',
  `type` varchar(50) DEFAULT NULL COMMENT '答案类型',
  `word` text COMMENT '文字',
	`voice` varchar(50) DEFAULT NULL COMMENT '语音ID',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
	`sortindex` int(11) DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='话术-一问多答表';



ALTER TABLE uk_que_survey_answer ADD multianswer tinyint(4) DEFAULT 0 COMMENT '启用一问多答';
ALTER TABLE uk_que_survey_answer ADD polling varchar(50) DEFAULT NULL COMMENT '轮询一问多答的方式（随机、顺序）';

ALTER TABLE uk_callcenter_router ADD worktimeitems text DEFAULT NULL COMMENT '工作时间';

CREATE TABLE `uk_callcenter_router_worktimeitems` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `routerid` varchar(50) DEFAULT NULL COMMENT '路由ID',
  `worktype` varchar(50) DEFAULT NULL COMMENT '工作类型',
  `beginhours` int(11) DEFAULT 0 COMMENT '开始小时',
  `beginmins` int(11) DEFAULT 0 COMMENT '开始分钟',
  `endhours` int(11) DEFAULT 0 COMMENT '结束小时',
  `endmins` int(11) DEFAULT 0 COMMENT '结束分钟',
  `action` varchar(50) DEFAULT NULL COMMENT '动作',
  `num` varchar(50) DEFAULT NULL COMMENT '转接号码',
  `voice` varchar(32) DEFAULT NULL COMMENT '语音id',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='路由工作时间表';

ALTER TABLE uk_callcenter_router_worktimeitems ADD datetype varchar(50) DEFAULT NULL COMMENT '日期类型';
ALTER TABLE uk_callcenter_router ADD nowork varchar(32) DEFAULT NULL COMMENT '非工作时间默认动作';
ALTER TABLE uk_callcenter_router ADD noworknum varchar(32) DEFAULT NULL COMMENT '非工作时间默认动作-转接号码';
ALTER TABLE uk_callcenter_router ADD noworkvoice varchar(32) DEFAULT NULL COMMENT '非工作时间默认动作-语音id';

ALTER TABLE uk_workservice_time ADD name varchar(50) DEFAULT NULL COMMENT '名称';

ALTER TABLE uk_callcenter_event ADD aitransans varchar(50) DEFAULT NULL COMMENT '答案ID';
ALTER TABLE uk_act_callnames ADD aitransans varchar(50) DEFAULT NULL COMMENT '答案ID';

ALTER TABLE uk_salespatter_multianswer ADD batinterrupt tinyint(4) DEFAULT 0 COMMENT '是否支持打断';
ALTER TABLE uk_salespatter_multianswer ADD batjump VARCHAR(50) DEFAULT NULL COMMENT '跳转';
ALTER TABLE uk_salespatter_multianswer ADD bathanguptype VARCHAR(50) DEFAULT NULL COMMENT '提示语类型';
ALTER TABLE uk_salespatter_multianswer ADD bathangupmsg text COMMENT '提示语文字';
ALTER TABLE uk_salespatter_multianswer ADD bathangupvoice VARCHAR(50) DEFAULT NULL COMMENT '提示语语音ID';

CREATE TABLE `uk_callcenter_hangup` (
	`id` varchar(32) NOT NULL COMMENT '主键ID',
	`callid` varchar(100) DEFAULT NULL COMMENT '通话id',
	`agent` varchar(100) DEFAULT NULL COMMENT '坐席工号或机器人工号',
	`hangupcase` varchar(32) DEFAULT NULL COMMENT '挂断原因',
	`hangupinitiator` varchar(32) DEFAULT NULL COMMENT '挂断方',
	`qusid` varchar(32) DEFAULT NULL COMMENT '问卷id',
	`qustitle` varchar(100) DEFAULT NULL COMMENT '问卷标题',
	`ansid` varchar(32) DEFAULT NULL COMMENT '答案id',
	`anstitle` varchar(100) DEFAULT NULL COMMENT '答案标题',
	`createtime` datetime DEFAULT NULL COMMENT '创建时间',
	`creater` varchar(32) DEFAULT NULL COMMENT '创建人',
	`orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
	`gateway` varchar(100) DEFAULT NULL COMMENT '网关名称',
	`duration` int(11) DEFAULT NULL COMMENT '通话时长',
	`asrtimes` int(11) DEFAULT '0' COMMENT 'asr次数',
	`ringduration` int(11) DEFAULT NULL COMMENT '振铃时长',
	`province` varchar(50) DEFAULT NULL COMMENT '来电号码归属省份',
	`city` varchar(50) DEFAULT NULL COMMENT '来电归属号码城市',
	`isp` varchar(50) DEFAULT NULL COMMENT '来电号码运营商',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='通话挂断表';

ALTER TABLE uk_callcenter_hangup ADD hangupqusid VARCHAR(50) DEFAULT NULL COMMENT '挂机问卷id';
ALTER TABLE uk_callcenter_hangup ADD hangupqustitle VARCHAR(100) DEFAULT NULL COMMENT '挂机问卷标题';
ALTER TABLE uk_callcenter_hangup ADD hangupansid VARCHAR(50) DEFAULT NULL COMMENT '挂机答案id';
ALTER TABLE uk_callcenter_hangup ADD hangupanstitle VARCHAR(100) DEFAULT NULL COMMENT '挂机答案标题';

ALTER TABLE uk_callcenter_event ADD autoquality tinyint DEFAULT 0 COMMENT '是否自动质检';

ALTER TABLE uk_columnproperties ADD strreplace text DEFAULT NULL COMMENT '字符替换';

ALTER TABLE uk_callcenter_event ADD spotqc tinyint DEFAULT 0 COMMENT '是否抽检';
ALTER TABLE uk_callcenter_event ADD spotqctime datetime DEFAULT NULL COMMENT '抽检时间';
ALTER TABLE uk_callcenter_event ADD spotqcsuccess tinyint DEFAULT 0 COMMENT '抽检是否成功';
ALTER TABLE uk_callcenter_event ADD appealqc tinyint DEFAULT 0 COMMENT '是否申诉质检';
ALTER TABLE uk_callcenter_event ADD arbitrateqc tinyint DEFAULT 0 COMMENT '是否仲裁质检';

CREATE TABLE `uk_qc_result_record` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `remarks` text COMMENT '质检备注',
  `adcom` text COMMENT '优点评语',
  `qacom` text COMMENT 'QA评语',
  `imcom` text COMMENT '改进评语',
  `score` int(11) DEFAULT '0' COMMENT '实际得分',
  `totalscore` int(11) DEFAULT '0' COMMENT '总分',
  `passscore` int(11) DEFAULT '0' COMMENT '合格分',
  `arithmetic` varchar(32) DEFAULT NULL COMMENT '算分机制(plus评分/minus扣分)',
  `missionid` varchar(32) DEFAULT NULL COMMENT '质检任务id',
  `dataid` varchar(50) DEFAULT NULL COMMENT '数据id',
  `qualityuser` varchar(32) DEFAULT NULL COMMENT '实际质检人',
  `status` varchar(32) DEFAULT NULL COMMENT '状态',
  `qualitytype` varchar(32) DEFAULT NULL COMMENT '质检类型（callevent通话/workorders工单/agentservice会话）',
  `isvp` int(11) DEFAULT '0' COMMENT '是否有否决权（1是/0否）',
  `isadcom` int(11) DEFAULT '0' COMMENT '是否有优点评语（1是/0否）',
  `isqacom` int(11) DEFAULT '0' COMMENT '是否QA评语（1是/0否）',
  `isimcom` int(11) DEFAULT '0' COMMENT '是否有改进评语（1是/0否）',
  `isrmk` int(11) DEFAULT '0' COMMENT '质检时是否有备注（1是/0否）',
  `isitemrmk` int(11) DEFAULT '0' COMMENT '质检项是否能填备注（1是/0否）',
  `isitemdir` int(11) DEFAULT '0' COMMENT '质检项是否有说明（1是/0否）',
  `qualityresultitem` text COMMENT '质检项json',
  `checktype` varchar(32) COMMENT '质检过程类型：spot抽检；appeal申诉质检；arbitrate仲裁质检；first首次质检',
  `autoquality` tinyint DEFAULT 0 COMMENT '是否自动质检',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='QC质检 - 质检结果记录';

ALTER TABLE uk_callcenter_hangup ADD called varchar(50) DEFAULT NULL COMMENT '被叫号码';

INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e871ba5c760171ba5edca2001b', '抽检', 'pub', 'A15_A02_A01_A01', 'ukewo', 'layui-icon', '402880fb678815060167881afc670023', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-04-27 14:41:51', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');
