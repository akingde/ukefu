package com.ukefu.webim.util.weixin.config;

import com.ukefu.webim.web.model.SNSAccount;

import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;

public class WeiXinConfiguration {
	
	private String snsid ;
	private WxMpMessageRouter router ;
	private WxMpConfigStorage configStorage ;
	private WxMpService service ;
	
	private SNSAccount snsAccount ;

	public WxMpMessageRouter getRouter() {
		return router;
	}
	public void setRouter(WxMpMessageRouter router) {
		this.router = router;
	}
	public WxMpConfigStorage getConfigStorage() {
		return configStorage;
	}
	public void setConfigStorage(WxMpConfigStorage configStorage) {
		this.configStorage = configStorage;
	}
	public WxMpService getService() {
		return service;
	}
	public void setService(WxMpService service) {
		this.service = service;
	}
	public String getSnsid() {
		return snsid;
	}
	public void setSnsid(String snsid) {
		this.snsid = snsid;
	}
	public SNSAccount getSnsAccount() {
		return snsAccount;
	}
	public void setSnsAccount(SNSAccount snsAccount) {
		this.snsAccount = snsAccount;
	}

}
