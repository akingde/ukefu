package com.ukefu.webim.util.disruptor.ai;

import com.lmax.disruptor.RingBuffer;
import com.ukefu.util.event.AiEvent;
import com.ukefu.util.event.UserEvent;

public class AiEventProducer {
	private final RingBuffer<AiEvent> ringBuffer;

    public AiEventProducer(RingBuffer<AiEvent> ringBuffer)
    {
        this.ringBuffer = ringBuffer;
    }

    public void onData(UserEvent event){
    	 long id = ringBuffer.next();  // Grab the next sequence
         try{
        	 AiEvent aiEventData = ringBuffer.get(id);
        	 aiEventData.setEvent(event);
         }finally{
             ringBuffer.publish(id);
         }
    }
}
