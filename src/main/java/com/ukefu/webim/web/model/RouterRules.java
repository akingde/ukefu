package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.ukefu.util.UKTools;

@Entity
@Table(name = "uk_callcenter_router")
@org.hibernate.annotations.Proxy(lazy = false)
public class RouterRules implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3932323765657445180L;
	private String id = UKTools.getUUID();
	private String name;
	private String orgi;
	private String creater ;
	private String type;
	private String hostid ;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	private String regex ;
	private boolean allow ;
	private boolean falsebreak ;
	
	private boolean enablemoh ;		  //启用保持提示音
	private String moh ;				//保持音乐
	
	private boolean enabletransmusic; //启用转接提示音
	private String transmusic ;			//转接提示音
	
	private boolean enableworktime ;	//启用工作时间控制
	
	private String mday ;
	private String wday ;
	private String timeofday ;
	
	private boolean exportvars ;	//启用复制参数
	private boolean sysvars ;		//启用系统参数
	private boolean billing ;		//启用计费
	
	private String billingaccount ;	//计费账号
	
	
	private int routerinx ;		//路由规则 顺序
	
	private String routercontent ;	//路由规则内容
	private String field = "destination_number" ;			//字段 , 默认值destination_number

	private String extensioncontinue;
	
	private String worktimeitems ; //工作时间
	private String nowork;//非工作时间默认动作
	private String noworknum;//非工作时间默认动作-转接号码
	private String noworkvoice;//非工作时间默认动作-语音id
	
	private boolean enablecalltrans; //启用呼叫转移
	private String busyext;//坐席忙转号码
	private boolean enablecallagent; //启用坐席离线转手机
	private String notready;//无坐席在线的时候转入号码

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRegex() {
		return regex;
	}
	public void setRegex(String regex) {
		this.regex = regex;
	}
	public boolean isAllow() {
		return allow;
	}
	public void setAllow(boolean allow) {
		this.allow = allow;
	}
	public boolean isFalsebreak() {
		return falsebreak;
	}
	public void setFalsebreak(boolean falsebreak) {
		this.falsebreak = falsebreak;
	}
	public String getHostid() {
		return hostid;
	}
	public void setHostid(String hostid) {
		this.hostid = hostid;
	}
	public int getRouterinx() {
		return routerinx;
	}
	public void setRouterinx(int routerinx) {
		this.routerinx = routerinx;
	}
	public String getRoutercontent() {
		return routercontent;
	}
	public void setRoutercontent(String routercontent) {
		this.routercontent = routercontent;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}

	public String getExtensioncontinue() {
		return extensioncontinue;
	}

	public void setExtensioncontinue(String extensioncontinue) {
		this.extensioncontinue = extensioncontinue;
	}
	public boolean isEnablemoh() {
		return enablemoh;
	}
	public void setEnablemoh(boolean enablemoh) {
		this.enablemoh = enablemoh;
	}
	public boolean isEnabletransmusic() {
		return enabletransmusic;
	}
	public void setEnabletransmusic(boolean enabletransmusic) {
		this.enabletransmusic = enabletransmusic;
	}
	public boolean isEnableworktime() {
		return enableworktime;
	}
	public void setEnableworktime(boolean enableworktime) {
		this.enableworktime = enableworktime;
	}
	public String getMday() {
		return mday;
	}
	public void setMday(String mday) {
		this.mday = mday;
	}
	public String getWday() {
		return wday;
	}
	public void setWday(String wday) {
		this.wday = wday;
	}
	public String getTimeofday() {
		return timeofday;
	}
	public void setTimeofday(String timeofday) {
		this.timeofday = timeofday;
	}
	public String getMoh() {
		return moh;
	}
	public void setMoh(String moh) {
		this.moh = moh;
	}
	public String getTransmusic() {
		return transmusic;
	}
	public void setTransmusic(String transmusic) {
		this.transmusic = transmusic;
	}
	public boolean isExportvars() {
		return exportvars;
	}
	public void setExportvars(boolean exportvars) {
		this.exportvars = exportvars;
	}
	public boolean isSysvars() {
		return sysvars;
	}
	public void setSysvars(boolean sysvars) {
		this.sysvars = sysvars;
	}
	public boolean isBilling() {
		return billing;
	}
	public void setBilling(boolean billing) {
		this.billing = billing;
	}
	public String getBillingaccount() {
		return billingaccount;
	}
	public void setBillingaccount(String billingaccount) {
		this.billingaccount = billingaccount;
	}
	
	public String getWorktimeitems() {
		return worktimeitems;
	}
	public void setWorktimeitems(String worktimeitems) {
		this.worktimeitems = worktimeitems;
	}
	
	public String getNowork() {
		return nowork;
	}
	public void setNowork(String nowork) {
		this.nowork = nowork;
	}
	public String getNoworknum() {
		return noworknum;
	}
	public void setNoworknum(String noworknum) {
		this.noworknum = noworknum;
	}
	public String getNoworkvoice() {
		return noworkvoice;
	}
	public void setNoworkvoice(String noworkvoice) {
		this.noworkvoice = noworkvoice;
	}
	public boolean isEnablecalltrans() {
		return enablecalltrans;
	}
	public void setEnablecalltrans(boolean enablecalltrans) {
		this.enablecalltrans = enablecalltrans;
	}
	public String getBusyext() {
		return busyext;
	}
	public void setBusyext(String busyext) {
		this.busyext = busyext;
	}
	public boolean isEnablecallagent() {
		return enablecallagent;
	}
	public void setEnablecallagent(boolean enablecallagent) {
		this.enablecallagent = enablecallagent;
	}
	public String getNotready() {
		return notready;
	}
	public void setNotready(String notready) {
		this.notready = notready;
	}
}
