package com.ukefu.webim.web.model;

public class ColumnPropertiesStrReplace implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4839215474511319999L;
	
	private String word;
	private String repword;
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getRepword() {
		return repword;
	}
	public void setRepword(String repword) {
		this.repword = repword;
	}
	
	
}
