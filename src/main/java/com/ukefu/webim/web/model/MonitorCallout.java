package com.ukefu.webim.web.model;

public class MonitorCallout {
	
	private String username;
	private String usertype;//agent坐席，ai机器人
	private String callouttotal;//外呼通话总量
	private String aitotal;//机器人外呼总数
	private String transtotal;//转人工总数
	private String transsuccess;//机器人转人工成功总数
	
	private String aitransdurationavg;//转人工平均时长
	
	private String transdurationsum;//人工坐席通话总时长
	private String transdurationavg;//人工坐席平均通话时长
	
	private String aidurationsum;//机器人通话总时长
	private String aiasrtimessum;//调用asr总次数
	private String aiasrtimesavg;//调用asr平均次数
	private String aidurationavg;//机器人平均通话时长
	private String airingdurationavg;//振铃平均时长
	
	private String qualitytotal;//质检总量
	private String qualitysuccess;//质检成功量
	private String qualityfail;//质检失败量
	private String qualitypre;//质检率
	private String qualityno;//未质检量
	private String qualityshold;//应质检总量
	
	private String agentdurationavg;//人工坐席通话平均时长
	private String invitation;//成功件
	private String invitationtotal;//邀约量
	private String aicutin;//接通量
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCallouttotal() {
		return callouttotal;
	}
	public void setCallouttotal(String callouttotal) {
		this.callouttotal = callouttotal;
	}
	public String getAitotal() {
		return aitotal;
	}
	public void setAitotal(String aitotal) {
		this.aitotal = aitotal;
	}
	public String getTranstotal() {
		return transtotal;
	}
	public void setTranstotal(String transtotal) {
		this.transtotal = transtotal;
	}
	public String getTranssuccess() {
		return transsuccess;
	}
	public void setTranssuccess(String transsuccess) {
		this.transsuccess = transsuccess;
	}
	public String getTransdurationavg() {
		return transdurationavg;
	}
	public void setTransdurationavg(String transdurationavg) {
		this.transdurationavg = transdurationavg;
	}
	public String getAitransdurationavg() {
		return aitransdurationavg;
	}
	public void setAitransdurationavg(String aitransdurationavg) {
		this.aitransdurationavg = aitransdurationavg;
	}
	public String getTransdurationsum() {
		return transdurationsum;
	}
	public void setTransdurationsum(String transdurationsum) {
		this.transdurationsum = transdurationsum;
	}
	public String getAidurationsum() {
		return aidurationsum;
	}
	public void setAidurationsum(String aidurationsum) {
		this.aidurationsum = aidurationsum;
	}
	public String getAiasrtimessum() {
		return aiasrtimessum;
	}
	public void setAiasrtimessum(String aiasrtimessum) {
		this.aiasrtimessum = aiasrtimessum;
	}
	public String getAiasrtimesavg() {
		return aiasrtimesavg;
	}
	public void setAiasrtimesavg(String aiasrtimesavg) {
		this.aiasrtimesavg = aiasrtimesavg;
	}
	public String getAidurationavg() {
		return aidurationavg;
	}
	public void setAidurationavg(String aidurationavg) {
		this.aidurationavg = aidurationavg;
	}
	public String getAiringdurationavg() {
		return airingdurationavg;
	}
	public void setAiringdurationavg(String airingdurationavg) {
		this.airingdurationavg = airingdurationavg;
	}
	public String getQualitytotal() {
		return qualitytotal;
	}
	public void setQualitytotal(String qualitytotal) {
		this.qualitytotal = qualitytotal;
	}
	public String getQualitysuccess() {
		return qualitysuccess;
	}
	public void setQualitysuccess(String qualitysuccess) {
		this.qualitysuccess = qualitysuccess;
	}
	public String getQualityfail() {
		return qualityfail;
	}
	public void setQualityfail(String qualityfail) {
		this.qualityfail = qualityfail;
	}
	public String getQualitypre() {
		return qualitypre;
	}
	public void setQualitypre(String qualitypre) {
		this.qualitypre = qualitypre;
	}
	public String getQualityno() {
		return qualityno;
	}
	public void setQualityno(String qualityno) {
		this.qualityno = qualityno;
	}
	public String getQualityshold() {
		return qualityshold;
	}
	public void setQualityshold(String qualityshold) {
		this.qualityshold = qualityshold;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getAgentdurationavg() {
		return agentdurationavg;
	}
	public void setAgentdurationavg(String agentdurationavg) {
		this.agentdurationavg = agentdurationavg;
	}
	public String getInvitation() {
		return invitation;
	}
	public void setInvitation(String invitation) {
		this.invitation = invitation;
	}
	public String getAicutin() {
		return aicutin;
	}
	public void setAicutin(String aicutin) {
		this.aicutin = aicutin;
	}
	public String getInvitationtotal() {
		return invitationtotal;
	}
	public void setInvitationtotal(String invitationtotal) {
		this.invitationtotal = invitationtotal;
	}
	
	

}
