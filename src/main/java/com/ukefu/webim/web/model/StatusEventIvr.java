package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import com.ukefu.util.event.UserEvent;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventIvr implements Serializable,UserEvent{

	private static final long serialVersionUID = -5514665931637791341L;

	
	private String id ;
	private boolean ivr ;			//是否有IVR
	private Date ivrentertime ;		//IVR进入时间
	private Date ivrleavetime ;	//IVR离开时间
	private int ivrtime ;		//IVR停留时间
	
	private String orgi ;
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isIvr() {
		return ivr;
	}
	public void setIvr(boolean ivr) {
		this.ivr = ivr;
	}
	public Date getIvrentertime() {
		return ivrentertime;
	}
	public void setIvrentertime(Date ivrentertime) {
		this.ivrentertime = ivrentertime;
	}
	public Date getIvrleavetime() {
		return ivrleavetime;
	}
	public void setIvrleavetime(Date ivrleavetime) {
		this.ivrleavetime = ivrleavetime;
	}
	public int getIvrtime() {
		return ivrtime;
	}
	public void setIvrtime(int ivrtime) {
		this.ivrtime = ivrtime;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
}
