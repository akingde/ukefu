package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import com.ukefu.util.event.UserEvent;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventTrans implements Serializable,UserEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3557550447162132063L;

	private String id ;

	private String tranid;//语音转写任务ID
	
	private String transtatus;
	
	private Date transbegin;//语音转写开始时间
	private Date transend;	//语音转写结束时间
	private String transtime;//语音转写用时
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTranid() {
		return tranid;
	}
	public void setTranid(String tranid) {
		this.tranid = tranid;
	}
	public String getTranstatus() {
		return transtatus;
	}
	public void setTranstatus(String transtatus) {
		this.transtatus = transtatus;
	}
	public Date getTransbegin() {
		return transbegin;
	}
	public void setTransbegin(Date transbegin) {
		this.transbegin = transbegin;
	}
	public Date getTransend() {
		return transend;
	}
	public void setTransend(Date transend) {
		this.transend = transend;
	}
	public String getTranstime() {
		return transtime;
	}
	public void setTranstime(String transtime) {
		this.transtime = transtime;
	}



}
