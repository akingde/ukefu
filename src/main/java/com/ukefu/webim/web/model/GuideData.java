package com.ukefu.webim.web.model;

import java.util.List;

/**
 * 
 */
public class GuideData implements java.io.Serializable{


	private static final long serialVersionUID = 1115593425069549681L;
	
	private String title  ;	//展示标题
	private String content;	//回复内容
	private String type;	//类型
	private String url;		//链接

	private String icon ;	//展示图标
	private String style ;	//自定义样式
	
	private List<?> list ;
	
	private String[] protype ;
	private String[] protitle ;
	private String[] prourl ;
	private String[] proicon ;
	private String[] prostyle;
	
	private String[] weltype;
	private String[] weltitle;
	private String[] welurl;
	private String[] welicon;
	private String[] welstyle;
	
	private String[] captype;
	private String[] captitle;
	private String[] capurl;
	private String[] capicon;
	private String[] capstyle;
	
	private String[] shorttype;
	private String[] shorttitle;
	private String[] shorturl;
	private String[] shorticon;
	private String[] shortstyle;
	
	private String[] displaytype;
	private String[] tabname;
	private String[] taburl;
	
	private String item;
	private String score;
	private String[] items;
	private String[] scores;
	
	private String compare;
	private String[] compares;
	
	private String[] titles;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public List<?> getList() {
		return list;
	}
	public void setList(List<?> list) {
		this.list = list;
	}
	public String[] getProtype() {
		return protype;
	}
	public void setProtype(String[] protype) {
		this.protype = protype;
	}
	public String[] getProtitle() {
		return protitle;
	}
	public void setProtitle(String[] protitle) {
		this.protitle = protitle;
	}
	public String[] getProurl() {
		return prourl;
	}
	public void setProurl(String[] prourl) {
		this.prourl = prourl;
	}
	public String[] getProicon() {
		return proicon;
	}
	public void setProicon(String[] proicon) {
		this.proicon = proicon;
	}
	public String[] getProstyle() {
		return prostyle;
	}
	public void setProstyle(String[] prostyle) {
		this.prostyle = prostyle;
	}
	public String[] getWeltype() {
		return weltype;
	}
	public void setWeltype(String[] weltype) {
		this.weltype = weltype;
	}
	public String[] getWeltitle() {
		return weltitle;
	}
	public void setWeltitle(String[] weltitle) {
		this.weltitle = weltitle;
	}
	public String[] getWelurl() {
		return welurl;
	}
	public void setWelurl(String[] welurl) {
		this.welurl = welurl;
	}
	public String[] getWelicon() {
		return welicon;
	}
	public void setWelicon(String[] welicon) {
		this.welicon = welicon;
	}
	public String[] getWelstyle() {
		return welstyle;
	}
	public void setWelstyle(String[] welstyle) {
		this.welstyle = welstyle;
	}
	public String[] getCaptype() {
		return captype;
	}
	public void setCaptype(String[] captype) {
		this.captype = captype;
	}
	public String[] getCaptitle() {
		return captitle;
	}
	public void setCaptitle(String[] captitle) {
		this.captitle = captitle;
	}
	public String[] getCapurl() {
		return capurl;
	}
	public void setCapurl(String[] capurl) {
		this.capurl = capurl;
	}
	public String[] getCapicon() {
		return capicon;
	}
	public void setCapicon(String[] capicon) {
		this.capicon = capicon;
	}
	public String[] getCapstyle() {
		return capstyle;
	}
	public void setCapstyle(String[] capstyle) {
		this.capstyle = capstyle;
	}
	public String[] getShorttype() {
		return shorttype;
	}
	public void setShorttype(String[] shorttype) {
		this.shorttype = shorttype;
	}
	public String[] getShorttitle() {
		return shorttitle;
	}
	public void setShorttitle(String[] shorttitle) {
		this.shorttitle = shorttitle;
	}
	public String[] getShorturl() {
		return shorturl;
	}
	public void setShorturl(String[] shorturl) {
		this.shorturl = shorturl;
	}
	public String[] getShorticon() {
		return shorticon;
	}
	public void setShorticon(String[] shorticon) {
		this.shorticon = shorticon;
	}
	public String[] getShortstyle() {
		return shortstyle;
	}
	public void setShortstyle(String[] shortstyle) {
		this.shortstyle = shortstyle;
	}
	public String[] getDisplaytype() {
		return displaytype;
	}
	public void setDisplaytype(String[] displaytype) {
		this.displaytype = displaytype;
	}
	public String[] getTabname() {
		return tabname;
	}
	public void setTabname(String[] tabname) {
		this.tabname = tabname;
	}
	public String[] getTaburl() {
		return taburl;
	}
	public void setTaburl(String[] taburl) {
		this.taburl = taburl;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String[] getItems() {
		return items;
	}
	public void setItems(String[] items) {
		this.items = items;
	}
	public String[] getScores() {
		return scores;
	}
	public void setScores(String[] scores) {
		this.scores = scores;
	}
	public String getCompare() {
		return compare;
	}
	public void setCompare(String compare) {
		this.compare = compare;
	}
	public String[] getCompares() {
		return compares;
	}
	public void setCompares(String[] compares) {
		this.compares = compares;
	}
	public String[] getTitles() {
		return titles;
	}
	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	
}
