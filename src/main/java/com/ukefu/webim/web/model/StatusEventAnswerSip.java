
package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import com.ukefu.util.event.UserEvent;

@Entity
@Table(name = "uk_callcenter_event_sip")
@Proxy(lazy = false)
public class StatusEventAnswerSip implements Serializable, UserEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2796276564445713776L;

	private String id ;
	private Date updatetime = new Date() ;
	
	private String orgi ;
	private String answersip ;
	
	
	public StatusEventAnswerSip() {}
	public StatusEventAnswerSip(String id , String orgi ,Date updatetime , String answersip) {
		this.id = id ;
		this.orgi = orgi ;
		this.updatetime = updatetime ;
		this.answersip= answersip ;
	}
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getAnswersip() {
		return answersip;
	}
	public void setAnswersip(String answersip) {
		this.answersip = answersip;
	}
}
