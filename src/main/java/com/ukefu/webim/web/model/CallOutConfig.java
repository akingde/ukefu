package com.ukefu.webim.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.ukefu.util.UKTools;
import com.ukefu.webim.util.OnlineUserUtils;

@Entity
@Table(name = "uk_act_config")
@org.hibernate.annotations.Proxy(lazy = false)
public class CallOutConfig implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3932323765657445180L;
	private String id;
	private String name;
	private String orgi;
	private String creater ;
	private String type;
	
	private String dataid ;		//部门ID，将来按照部门扩展，每个部门可以有团队长独立设置 部门策略或其他标识
	
	private Date createtime = new Date();
	private Date updatetime = new Date();
	private String username ;
	private boolean enablecallout ;	//启用自动外呼
	private int countdown ;			//外呼倒计时时长
	private boolean enabletagentthreads ;	//启用人工坐席并发控制
	private int agentthreads ;				//人工坐席外呼并发数量
	
	private boolean enabletaithreads ;		//启用机器人并发外呼限制
	private int aithreads ;					//机器人外呼并发数量

	private boolean enablefthreads ;		//启用预测式外呼并发限制
	private int fthreads ;					//预测式外呼并发数量
	
	
	private boolean enableauto ;			//启用预览式外呼全自动功能
	
	private boolean forecast ;				//是否启用预测式外呼功能
	private int forecastratio ;				//预测阀值
	
	private int fmaxavgtime ;			//预计最大平均通话时长
	private int fminavgtime ;			//预计最小平均通话时长
	private int favgaftertime ;			//预计平均后处理时长
	
	private boolean autoready ;			//登录后自动进入等待名单状态
	
	private String defaultvalue ;	//默认 allow
	private String strategy;		//策略
	
	private boolean previewautocallout ;	//启用主动预览下的自动外呼功能
	
	private boolean appointment;//来点弹屏页，是否预约项的默认值（0是/1否）

	private boolean enablebusinesscustom; //启用业务自定义功能

	private boolean enableagentcustomtabs;//启用后，在线客服坐席端的右侧，展示自定义的页签内容
	private String dataagentcustomtabs;//自定义页签集，json
	
	private List<GuideData> dataagentcustomtabsList;
	
	private boolean enablecuswodstatus;//启用自定义工单状态
	private String cuswodstatusdic;//自定义工单状态字典项
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getDefaultvalue() {
		return defaultvalue;
	}
	public void setDefaultvalue(String defaultvalue) {
		this.defaultvalue = defaultvalue;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isEnablecallout() {
		return enablecallout;
	}
	public void setEnablecallout(boolean enablecallout) {
		this.enablecallout = enablecallout;
	}
	public int getCountdown() {
		return countdown;
	}
	public void setCountdown(int countdown) {
		this.countdown = countdown;
	}
	public boolean isEnabletagentthreads() {
		return enabletagentthreads;
	}
	public void setEnabletagentthreads(boolean enabletagentthreads) {
		this.enabletagentthreads = enabletagentthreads;
	}
	public int getAgentthreads() {
		return agentthreads;
	}
	public void setAgentthreads(int agentthreads) {
		this.agentthreads = agentthreads;
	}
	public boolean isEnabletaithreads() {
		return enabletaithreads;
	}
	public void setEnabletaithreads(boolean enabletaithreads) {
		this.enabletaithreads = enabletaithreads;
	}
	public int getAithreads() {
		return aithreads;
	}
	public void setAithreads(int aithreads) {
		this.aithreads = aithreads;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public boolean isPreviewautocallout() {
		return previewautocallout;
	}
	public void setPreviewautocallout(boolean previewautocallout) {
		this.previewautocallout = previewautocallout;
	}
	public boolean isForecast() {
		return forecast;
	}
	public void setForecast(boolean forecast) {
		this.forecast = forecast;
	}
	public int getForecastratio() {
		return forecastratio;
	}
	public void setForecastratio(int forecastratio) {
		this.forecastratio = forecastratio;
	}
	public int getFmaxavgtime() {
		return fmaxavgtime;
	}
	public void setFmaxavgtime(int fmaxavgtime) {
		this.fmaxavgtime = fmaxavgtime;
	}
	public int getFminavgtime() {
		return fminavgtime;
	}
	public void setFminavgtime(int fminavgtime) {
		this.fminavgtime = fminavgtime;
	}
	public int getFavgaftertime() {
		return favgaftertime;
	}
	public void setFavgaftertime(int favgaftertime) {
		this.favgaftertime = favgaftertime;
	}
	public boolean isEnableauto() {
		return enableauto;
	}
	public void setEnableauto(boolean enableauto) {
		this.enableauto = enableauto;
	}
	public boolean isEnablefthreads() {
		return enablefthreads;
	}
	public void setEnablefthreads(boolean enablefthreads) {
		this.enablefthreads = enablefthreads;
	}
	public int getFthreads() {
		return fthreads;
	}
	public void setFthreads(int fthreads) {
		this.fthreads = fthreads;
	}
	public boolean isAppointment() {
		return appointment;
	}
	public void setAppointment(boolean appointment) {
		this.appointment = appointment;
	}
	public boolean isAutoready() {
		return autoready;
	}
	public void setAutoready(boolean autoready) {
		this.autoready = autoready;
	}

	public boolean isEnableagentcustomtabs() {
		return enableagentcustomtabs;
	}

	public void setEnableagentcustomtabs(boolean enableagentcustomtabs) {
		this.enableagentcustomtabs = enableagentcustomtabs;
	}

	public String getDataagentcustomtabs() {
		return dataagentcustomtabs;
	}

	public void setDataagentcustomtabs(String dataagentcustomtabs) {
		this.dataagentcustomtabs = dataagentcustomtabs;
	}

	public boolean isEnablebusinesscustom() {
		return enablebusinesscustom;
	}

	public void setEnablebusinesscustom(boolean enablebusinesscustom) {
		this.enablebusinesscustom = enablebusinesscustom;
	}
	public boolean isEnablecuswodstatus() {
		return enablecuswodstatus;
	}
	public void setEnablecuswodstatus(boolean enablecuswodstatus) {
		this.enablecuswodstatus = enablecuswodstatus;
	}
	public String getCuswodstatusdic() {
		return cuswodstatusdic;
	}
	public void setCuswodstatusdic(String cuswodstatusdic) {
		this.cuswodstatusdic = cuswodstatusdic;
	}
	@Transient
	public List<GuideData> getDataagentcustomtabsList() {
		
		
		List<GuideData> list = null ;
		  if(!StringUtils.isBlank(this.dataagentcustomtabs)) {
		   try {
		    list = OnlineUserUtils.objectMapper.readValue(this.dataagentcustomtabs, UKTools.getCollectionType(ArrayList.class, GuideData.class))  ;
		   } catch (Exception e) {
		    e.printStackTrace();
		   }
		  }
		  return list ;
		
	}
	public void setDataagentcustomtabsList(List<GuideData> dataagentcustomtabsList) {
		this.dataagentcustomtabsList = dataagentcustomtabsList;
	}
}
