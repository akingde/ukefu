package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import com.ukefu.util.event.UserEvent;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventRobot implements Serializable,UserEvent{

	private static final long serialVersionUID = -5514665931637791341L;

	private String id ;

	private Integer levelscore;
	private String level;
	private Integer focustimes;
	
	private boolean ai ;		//是否机器人呼叫
	private String aieventid ;	//机器人呼叫ID
	private int asrtimes ;		//asr次数
	private int ttstimes ;		//tts次数
	
	private int timeouttimes ;		//tts次数
	private int errortimes ;		//tts次数
	private int nmlinetimes ;		//tts次数
	
	private String aitransqus;	//转接问题ID
	private Date aitranstime ;	//转接时间
	private long aitransduration ;	//转接时通话时长
	
	
	private boolean aitrans ;		//转接人工

	private String processid;//话术or问卷id\

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


	public Integer getLevelscore() {
		return levelscore;
	}

	public void setLevelscore(Integer levelscore) {
		this.levelscore = levelscore;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Integer getFocustimes() {
		return focustimes;
	}

	public void setFocustimes(Integer focustimes) {
		this.focustimes = focustimes;
	}

	public String getProcessid() {
		return processid;
	}

	public void setProcessid(String processid) {
		this.processid = processid;
	}
	public boolean isAi() {
		return ai;
	}
	public void setAi(boolean ai) {
		this.ai = ai;
	}
	public String getAieventid() {
		return aieventid;
	}
	public void setAieventid(String aieventid) {
		this.aieventid = aieventid;
	}
	public int getAsrtimes() {
		return asrtimes;
	}
	public void setAsrtimes(int asrtimes) {
		this.asrtimes = asrtimes;
	}
	public int getTtstimes() {
		return ttstimes;
	}
	public void setTtstimes(int ttstimes) {
		this.ttstimes = ttstimes;
	}
	public int getTimeouttimes() {
		return timeouttimes;
	}
	public void setTimeouttimes(int timeouttimes) {
		this.timeouttimes = timeouttimes;
	}
	public int getErrortimes() {
		return errortimes;
	}
	public void setErrortimes(int errortimes) {
		this.errortimes = errortimes;
	}
	public int getNmlinetimes() {
		return nmlinetimes;
	}
	public void setNmlinetimes(int nmlinetimes) {
		this.nmlinetimes = nmlinetimes;
	}
	public boolean isAitrans() {
		return aitrans;
	}
	public void setAitrans(boolean aitrans) {
		this.aitrans = aitrans;
	}
	public String getAitransqus() {
		return aitransqus;
	}
	public void setAitransqus(String aitransqus) {
		this.aitransqus = aitransqus;
	}
	public Date getAitranstime() {
		return aitranstime;
	}
	public void setAitranstime(Date aitranstime) {
		this.aitranstime = aitranstime;
	}
	public long getAitransduration() {
		return aitransduration;
	}
	public void setAitransduration(long aitransduration) {
		this.aitransduration = aitransduration;
	}
	
}
