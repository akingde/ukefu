package com.ukefu.webim.web.model;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventRecord implements Serializable,UserEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6811424879435316201L;

	private String id ;

	private Date updatetime = new Date() ;
	
	private boolean record ;//是否录音
	
	private Date startrecord ;//开始录音时间
	private Date endrecord ;//结束录音时间
	private int recordtime ;//录音时长
	private String recordfile ;//开始录音时间
	private String recordfilename ;//结束录音时间

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	@Column(name="srecord")
	public boolean isRecord() {
		return record;
	}

	public void setRecord(boolean record) {
		this.record = record;
	}

	public Date getStartrecord() {
		return startrecord;
	}

	public void setStartrecord(Date startrecord) {
		this.startrecord = startrecord;
	}

	public Date getEndrecord() {
		return endrecord;
	}

	public void setEndrecord(Date endrecord) {
		this.endrecord = endrecord;
	}

	public int getRecordtime() {
		return recordtime;
	}

	public void setRecordtime(int recordtime) {
		this.recordtime = recordtime;
	}

	public String getRecordfile() {
		return recordfile;
	}

	public void setRecordfile(String recordfile) {
		this.recordfile = recordfile;
	}

	public String getRecordfilename() {
		return recordfilename;
	}

	public void setRecordfilename(String recordfilename) {
		this.recordfilename = recordfilename;
	}

}
