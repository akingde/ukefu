package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/**
 * 动态路由配置表，启用动态路由后，可以根据归属地来选择出局线路
 *
 */
@Entity
@Table(name = "uk_callcenter_siptrunk_dynamic")
@org.hibernate.annotations.Proxy(lazy = false)
public class CallcenterSipTrunkDynamic implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1115593425069549681L;
	
	private String id  ;
	private String siptrunkid;//SIP网关ID
	private String hostid;//语音平台服务器ID

	private Date createtime ;
	
	private String province;//省
	private String city;//市
	private String area;//县
	
	private String gatewayid;//归属地指定绑定的网关
	
	private String orgi ;
	
	

	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSiptrunkid() {
		return siptrunkid;
	}
	public void setSiptrunkid(String siptrunkid) {
		this.siptrunkid = siptrunkid;
	}
	public String getHostid() {
		return hostid;
	}
	public void setHostid(String hostid) {
		this.hostid = hostid;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getGatewayid() {
		return gatewayid;
	}
	public void setGatewayid(String gatewayid) {
		this.gatewayid = gatewayid;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	
	
	
}
