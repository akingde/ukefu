package com.ukefu.webim.web.model;

public interface TransAgentProcess {
	public String getId();
	public String getOrgi();
	public int getQuetype();
	public String getTrans();
	public boolean isEnabletransfercon() ;
	public String getTransferconditions() ;
	public String getTransferequal() ;
	public String getTransfervalue() ;
	public String getTransferactivity();
	public String getTransferwvtype() ;
	public String getTransfervoice() ;
	public String getTransferword() ;
	public String getTransnode() ;
	
}
