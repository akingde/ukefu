package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventBridge implements Serializable,UserEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6274045807300507192L;

	private String id ;

	private String servicestatus ;	//通话状态

	private String bridgeid ;			//桥接对方ID
	private boolean bridge ;			//是否桥接
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getServicestatus() {
		return servicestatus;
	}

	public void setServicestatus(String servicestatus) {
		this.servicestatus = servicestatus;
	}

	public String getBridgeid() {
		return bridgeid;
	}

	public void setBridgeid(String bridgeid) {
		this.bridgeid = bridgeid;
	}

	public boolean isBridge() {
		return bridge;
	}

	public void setBridge(boolean bridge) {
		this.bridge = bridge;
	}
}
