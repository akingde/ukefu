package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 性别年龄识别保存类
 *
 */
@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventIgr implements Serializable,UserEvent{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id ;

	private boolean igr;		//是否进行性别年龄识别
	private String igrage;		//识别引擎返回 - 表示识别的年龄[0：middle(12~40岁) 1：child（0~12岁）2：old（40岁以上）]
	private String igrchild;	//识别引擎返回 - 表示识别为儿童的概率值，儿童、中年、老年概率值最大的为最终结果	
	private String igrmiddle;	//识别引擎返回 - 表示识别为中年的概率值，儿童、中年、老年概率值最大的为最终结果
	private String igrold;		//识别引擎返回 - 表示识别为老年的概率值，儿童、中年、老年概率值最大的为最终结果
	
	private String igrgender;	//识别引擎返回 - 表示识别的性别 [0：女性 1：男性]
	private String igrfemale;	//识别引擎返回 - 表示识别为女声的概率值，女声、男声概率值较大的为最终结果
	private String igrmale;		//识别引擎返回 - 表示识别为男声的概率值，女声、男声概率值较大的为最终结
	
	private Date igrbegin;		//开始识别
	private Date igrend;		//结束识别

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isIgr() {
		return igr;
	}
	public void setIgr(boolean igr) {
		this.igr = igr;
	}
	public String getIgrage() {
		return igrage;
	}
	public void setIgrage(String igrage) {
		this.igrage = igrage;
	}
	public String getIgrchild() {
		return igrchild;
	}
	public void setIgrchild(String igrchild) {
		this.igrchild = igrchild;
	}
	public String getIgrmiddle() {
		return igrmiddle;
	}
	public void setIgrmiddle(String igrmiddle) {
		this.igrmiddle = igrmiddle;
	}
	public String getIgrold() {
		return igrold;
	}
	public void setIgrold(String igrold) {
		this.igrold = igrold;
	}
	public String getIgrgender() {
		return igrgender;
	}
	public void setIgrgender(String igrgender) {
		this.igrgender = igrgender;
	}
	public String getIgrfemale() {
		return igrfemale;
	}
	public void setIgrfemale(String igrfemale) {
		this.igrfemale = igrfemale;
	}
	public String getIgrmale() {
		return igrmale;
	}
	public void setIgrmale(String igrmale) {
		this.igrmale = igrmale;
	}
	public Date getIgrbegin() {
		return igrbegin;
	}
	public void setIgrbegin(Date igrbegin) {
		this.igrbegin = igrbegin;
	}
	public Date getIgrend() {
		return igrend;
	}
	public void setIgrend(Date igrend) {
		this.igrend = igrend;
	}

	
}
