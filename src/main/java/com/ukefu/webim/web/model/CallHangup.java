package com.ukefu.webim.web.model;

import com.ukefu.util.UKTools;
import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "uk_callcenter_hangup")
@Proxy(lazy = false)
public class CallHangup implements Serializable,UserEvent{


	/**
	 * 
	 */
	private static final long serialVersionUID = 645346425019559670L;
	
	private String id =UKTools.getUUID();
	private String callid ;//通话id
	private String agent ;//坐席工号或机器人工号
	private String called ;	//被叫号码
	private String hangupcase ;		//挂断原因	
	private String hangupinitiator ;//挂断发起方
	private String qusid;//问卷id
	private String qustitle;//问卷标题
	private String ansid;//答案id
	private String anstitle;//答案标题
	private Date createtime = new Date();
	private String creater;
	private String orgi;
	
	private String gateway;//网关
	private int duration;//通话时长
	private String asrtimes;//ASR次数
	private int ringduration;//振铃时长
	private String city;//城市
	private String province;//省份
	private String isp; //运营商
	
	private String hangupqusid;//挂机问卷id
	private String hangupqustitle;//挂机问卷标题
	private String hangupansid;//挂机答案id
	private String hangupanstitle;//挂机答案标题
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCallid() {
		return callid;
	}
	public void setCallid(String callid) {
		this.callid = callid;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getHangupcase() {
		return hangupcase;
	}
	public void setHangupcase(String hangupcase) {
		this.hangupcase = hangupcase;
	}
	public String getHangupinitiator() {
		return hangupinitiator;
	}
	public void setHangupinitiator(String hangupinitiator) {
		this.hangupinitiator = hangupinitiator;
	}
	public String getQusid() {
		return qusid;
	}
	public void setQusid(String qusid) {
		this.qusid = qusid;
	}
	public String getQustitle() {
		return qustitle;
	}
	public void setQustitle(String qustitle) {
		this.qustitle = qustitle;
	}
	public String getAnsid() {
		return ansid;
	}
	public void setAnsid(String ansid) {
		this.ansid = ansid;
	}
	public String getAnstitle() {
		return anstitle;
	}
	public void setAnstitle(String anstitle) {
		this.anstitle = anstitle;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getAsrtimes() {
		return asrtimes;
	}
	public void setAsrtimes(String asrtimes) {
		this.asrtimes = asrtimes;
	}
	public int getRingduration() {
		return ringduration;
	}
	public void setRingduration(int ringduration) {
		this.ringduration = ringduration;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getIsp() {
		return isp;
	}
	public void setIsp(String isp) {
		this.isp = isp;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getHangupqusid() {
		return hangupqusid;
	}
	public void setHangupqusid(String hangupqusid) {
		this.hangupqusid = hangupqusid;
	}
	public String getHangupqustitle() {
		return hangupqustitle;
	}
	public void setHangupqustitle(String hangupqustitle) {
		this.hangupqustitle = hangupqustitle;
	}
	public String getHangupansid() {
		return hangupansid;
	}
	public void setHangupansid(String hangupansid) {
		this.hangupansid = hangupansid;
	}
	public String getHangupanstitle() {
		return hangupanstitle;
	}
	public void setHangupanstitle(String hangupanstitle) {
		this.hangupanstitle = hangupanstitle;
	}
	public String getCalled() {
		return called;
	}
	public void setCalled(String called) {
		this.called = called;
	}
}
