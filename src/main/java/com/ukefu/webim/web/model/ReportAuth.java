package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 报表权限表
 *
 */
@Entity
@Table(name = "uk_report_auth")
@org.hibernate.annotations.Proxy(lazy = false)
public class ReportAuth implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id ;
	private String creater;
	private Date createtime  = new Date();//创建时间
	private String orgi ;
	private String type ;//报表分类或报表
	private String report ;//报表id
	private String dicid ;//报表类型id
	private String authorgan;//授权部门
	private String authrole;//授权角色
	private String authuser;//授权角色
	
	private String datadicid;
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getDicid() {
		return dicid;
	}
	public void setDicid(String dicid) {
		this.dicid = dicid;
	}
	public String getAuthorgan() {
		return authorgan;
	}
	public void setAuthorgan(String authorgan) {
		this.authorgan = authorgan;
	}
	public String getAuthrole() {
		return authrole;
	}
	public void setAuthrole(String authrole) {
		this.authrole = authrole;
	}
	public String getAuthuser() {
		return authuser;
	}
	public void setAuthuser(String authuser) {
		this.authuser = authuser;
	}
	public String getDatadicid() {
		return datadicid;
	}
	public void setDatadicid(String datadicid) {
		this.datadicid = datadicid;
	}
	
	
}
