package com.ukefu.webim.web.handler.api.request;

import java.io.Serializable;
import java.util.List;

public class RequestValues<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private QueryParams query ;
	private T data ;
	private List<T> list ;
	public QueryParams getQuery() {
		return query;
	}
	public void setQuery(QueryParams query) {
		this.query = query;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
}
