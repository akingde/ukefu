package com.ukefu.webim.web.handler.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.webim.service.es.TopicRepository;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.handler.api.request.SearchData;
import com.ukefu.webim.web.model.Topic;

@RestController
@RequestMapping("/api/xiaoe/topic")
@Api(value = "智能机器人", description = "知识库维护")
public class ApiXiaoeTopicController extends Handler{

	@Autowired
	private TopicRepository topicRepository;
	
	/**
	 * 知识库管理功能
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 */
	@RequestMapping( method = RequestMethod.GET)
	@Menu(type = "apps" , subtype = "topic" , access = true)
	@ApiOperation("返回知识库列表，支持分页，分页参数为 p=1&ps=50，默认分页尺寸为 20条每页")
    public ResponseEntity<RestResult> list(HttpServletRequest request , @Valid String cate, @Valid String q) {
		Page<Topic> topicList = topicRepository.getTopicByCateAndOrgi(!StringUtils.isBlank(cate) ? cate : UKDataContext.DEFAULT_TYPE , super.getOrgi(request),q,  super.getP(request), super.getPs(request)) ;
        return new ResponseEntity<>(new RestResult(RestResultType.OK, new SearchData<Topic>(topicList)), HttpStatus.OK);
    }
	
	/**
	 * 新增或修改用户用户 ，在修改用户信息的时候，如果用户 密码未改变，请设置为 NULL
	 * @param request
	 * @param user
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@Menu(type = "apps" , subtype = "topic" , access = true)
	@ApiOperation("新增/编辑知识内容")
    public ResponseEntity<RestResult> put(HttpServletRequest request , @Valid Topic topic) {
    	if(topic != null && !StringUtils.isBlank(topic.getTitle())){
    		if(StringUtils.isBlank(topic.getCate())){
    			topic.setCate(UKDataContext.DEFAULT_TYPE);
    		}
    		topic.setOrgi(super.getOrgi(request));
    		topicRepository.save(topic) ;
    	}
        return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
    }
	
	/**
	 * 删除用户，只提供 按照用户ID删除 ， 并且，不能删除系统管理员
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	@Menu(type = "apps" , subtype = "topic" , access = true)
	@ApiOperation("删除知识库内容")
    public ResponseEntity<RestResult> delete(HttpServletRequest request , @Valid String id) {
		RestResult result = new RestResult(RestResultType.OK) ; 
    	Topic topic = null ;
    	if(!StringUtils.isBlank(id)){
    		topic = topicRepository.findByIdAndOrgi(id,super.getOrgi(request)) ;
    		if(topic!=null){	
    			topicRepository.delete(topic);
    		}else{
    			result.setStatus(RestResultType.XIAOE_TOPIC_DELETE);
    		}
    	}
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}