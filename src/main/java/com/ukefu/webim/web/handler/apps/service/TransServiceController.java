package com.ukefu.webim.web.handler.apps.service;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.webim.web.handler.Handler;

@Controller
@RequestMapping("/trans/resource")
public class TransServiceController extends Handler{
	
	/**
	 * 平台当前通话-录音播放
	 */
	@RequestMapping(value = "/voice")
    @Menu(access = false)
    public void play(ModelMap map , HttpServletRequest request , HttpServletResponse response ,String id ,String time,String sign) throws IOException {
		long endTime = Long.parseLong(time) + 1*60*1000 ;
		if(endTime >= new Date().getTime() && UKTools.md5(id+time).equals(sign)){
			if(!StringUtils.isBlank(id)){
				UKTools.playVoice(response, id, super.getOrgi(request), false, super.isEnabletneant());
			}
			return;
		}
    }
	
	/**
	 * 平台历史通话-录音播放
	 */
	@RequestMapping(value = "/history/voice/{id}")
    @Menu(access = false)
    public void historyplay(ModelMap map , HttpServletRequest request , HttpServletResponse response ,@PathVariable final String id ) throws IOException {
		if(!StringUtils.isBlank(id)){
			UKTools.playVoice(response, id, super.getOrgi(request), true, super.isEnabletneant());
		}
		return;
    }
	
	/**
	 * 租户平台当前通话-录音播放
	 */
	@RequestMapping(value = "/platform/voice/{id}")
    @Menu(access = false)
    public void pplay(ModelMap map , HttpServletRequest request , HttpServletResponse response ,@PathVariable final String id ) throws IOException {
		if(!StringUtils.isBlank(id)){
			UKTools.playVoice(response, id, null, false, super.isEnabletneant());
		}
		return;
    }
	
	/**
	 * 租户平台当前通话-录音播放
	 */
	@RequestMapping(value = "/platform/history/voice/{id}")
    @Menu(access = false)
    public void psplay(ModelMap map , HttpServletRequest request , HttpServletResponse response ,@PathVariable final String id ) throws IOException {
		if(!StringUtils.isBlank(id)){
			UKTools.playVoice(response, id, null, true, super.isEnabletneant());
		}
		return;
    }
}
