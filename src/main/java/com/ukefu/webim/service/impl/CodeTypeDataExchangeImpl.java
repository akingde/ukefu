package com.ukefu.webim.service.impl;
 
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ukefu.util.extra.DataExchangeInterface; 
import com.ukefu.webim.service.repository.TagRepository; 
import com.ukefu.webim.web.model.Tag;

@Service("codetype")
public class CodeTypeDataExchangeImpl implements DataExchangeInterface{
	@Autowired
	private TagRepository tagRepository ;
	
	public String getDataByIdAndOrgi(String id, String orgi){
		String result = "";
		StringBuffer str = null;
		if(StringUtils.isNotBlank(id) && id.length() > 32){
			str = new StringBuffer();
			String[] ids = id.split(",");
			if(ids != null){
				Tag tag = null;
				for(String temp: ids){
					tag = tagRepository.findByOrgiAndId(orgi,temp);
					if(tag != null){
						str.append(tag.getTag());
						str.append(",");
					}
				}
			}
			result = str.toString();
		}else{
			Tag tag = tagRepository.findByOrgiAndId(orgi,id);
			if(tag != null){
				result = tag.getTag();
			}
		}
		
		return result;
	}

	@Override
	public List<Tag> getListDataByIdAndOrgi(String id , String creater, String orgi) {
		return null;
	}
	
	public void process(Object data , String orgi) {
		
	}
}
