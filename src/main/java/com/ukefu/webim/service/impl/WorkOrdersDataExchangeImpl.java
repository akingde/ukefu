package com.ukefu.webim.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ukefu.util.extra.DataExchangeInterface;
import com.ukefu.webim.service.es.WorkOrdersRepository;
import com.ukefu.webim.web.model.WorkOrders;

@Service("workorders")
public class WorkOrdersDataExchangeImpl implements DataExchangeInterface{
	@Autowired
	private WorkOrdersRepository workOrdersRes ;
	
	public WorkOrders getDataByIdAndOrgi(String id, String orgi){
		return workOrdersRes.findByIdAndOrgi(id,orgi) ;
	}
	@Override
	public List<WorkOrders> getListDataByIdAndOrgi(String contactsid , String creater, String orgi) {
		return workOrdersRes.findByContactsAndOrgi(contactsid , creater, orgi) ;
	}
	
	public void process(Object data , String orgi) {
		
	}
	
}
