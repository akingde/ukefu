package com.ukefu.webim.service.quene;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.hazelcast.query.Predicate;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.freeswitch.model.CallCenterAgent;

public class CallCenterAgentOrgiFilterPredicate implements Predicate<String, CallCenterAgent> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1236581634096258855L;
	private String orgi ;
	private String actid = null;
	/**
	 * 
	 */
	public CallCenterAgentOrgiFilterPredicate(String orgi){
		this.orgi = orgi ;
	}
	public CallCenterAgentOrgiFilterPredicate(String orgi,String actid){
		this.orgi = orgi ;
		this.actid = actid;
	}
	public boolean apply(Map.Entry<String, CallCenterAgent> mapEntry) {
		boolean flag = mapEntry.getValue()!=null && !StringUtils.isBlank(orgi) && orgi.equals(mapEntry.getValue().getOrgi()) 
				&& mapEntry.getValue().isOffline()==false && !mapEntry.getValue().getWorkstatus().equals(UKDataContext.WorkStatusEnum.PREVIEW.toString());
		/*if(StringUtils.isNotBlank(actid)) {
			return flag && mapEntry.getValue().getActid()!=null && actid.equals(mapEntry.getValue().getActid());
		}*/
		return flag;
	}
}