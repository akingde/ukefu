package com.ukefu.webim.service.quene;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.hazelcast.query.Predicate;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.freeswitch.model.CallCenterAgent;

public class CallCenterPreviewActidFilterPredicate implements Predicate<String, CallCenterAgent> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1236581634096258855L;
	private String orgi ;
	private String actid=null;
	/**
	 * 
	 */
	public CallCenterPreviewActidFilterPredicate(String orgi){
		this.orgi = orgi ;
	}
	public CallCenterPreviewActidFilterPredicate(String orgi,String actid){
		this.orgi = orgi ;
		this.actid = actid;
	}
	public boolean apply(Map.Entry<String, CallCenterAgent> mapEntry) {
		boolean flag = mapEntry.getValue()!=null && !StringUtils.isBlank(orgi) && orgi.equals(mapEntry.getValue().getOrgi()) 
				&& UKDataContext.WorkStatusEnum.PREVIEW.toString().equals(mapEntry.getValue().getWorkstatus()) 
				&& mapEntry.getValue().isOffline()==false;
		/*if(StringUtils.isNotBlank(actid)) {
			return flag && mapEntry.getValue().getActid()!=null && actid.equals(mapEntry.getValue().getActid());
		}*/
		return flag;
	}
}