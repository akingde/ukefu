package com.ukefu.webim.service.quene;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.hazelcast.query.Predicate;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.freeswitch.model.CallCenterAgent;

public class CallCenterInCallOrgiActidFilterPredicate implements Predicate<String, CallCenterAgent> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1236581634096258855L;
	private String orgi ;
	private String actid=null;
	/**
	 * 
	 */
	public CallCenterInCallOrgiActidFilterPredicate(String orgi){
		this.orgi = orgi ;
	}
	public CallCenterInCallOrgiActidFilterPredicate(String orgi,String actid){
		this.orgi = orgi ;
		this.actid = actid;
	}
	public boolean apply(Map.Entry<String, CallCenterAgent> mapEntry) {
		boolean flag = mapEntry.getValue()!=null && !StringUtils.isBlank(orgi) && orgi.equals(mapEntry.getValue().getOrgi()) && UKDataContext.AgentStatusEnum.INCALL.toString().equals(mapEntry.getValue().getStatus());
		/*if(StringUtils.isNotBlank(actid)) {
			return flag && mapEntry.getValue().getActid()!=null && actid.equals(mapEntry.getValue().getActid());
		}*/
		return flag;
	}
}