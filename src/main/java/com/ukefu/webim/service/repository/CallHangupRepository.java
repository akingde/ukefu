package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.CallHangup;

public interface CallHangupRepository extends JpaRepository<CallHangup, String> {
	
	public abstract List<CallHangup> findByCallid(String callid);
}
