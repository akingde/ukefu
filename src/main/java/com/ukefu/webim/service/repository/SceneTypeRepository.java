package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.SceneType;

public abstract interface SceneTypeRepository extends JpaRepository<SceneType, String> {
	
	public abstract SceneType findByIdAndOrgi(String id, String orgi);

	public abstract int countByNameAndOrgi(String name, String orgi);
	
	public abstract List<SceneType> findByOrgi(String orgi) ;

}
