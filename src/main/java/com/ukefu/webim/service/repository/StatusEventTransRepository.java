package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventTrans;

public interface StatusEventTransRepository extends JpaRepository<StatusEventTrans, String> {
	
}
