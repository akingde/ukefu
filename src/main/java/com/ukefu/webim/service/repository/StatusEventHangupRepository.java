package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventHangup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventHangupRepository extends JpaRepository<StatusEventHangup, String> {

}
