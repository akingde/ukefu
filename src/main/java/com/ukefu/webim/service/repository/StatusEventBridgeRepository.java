package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventBridge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventBridgeRepository extends JpaRepository<StatusEventBridge, String> {

}
