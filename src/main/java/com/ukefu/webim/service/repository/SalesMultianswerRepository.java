package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.SalesMultianswer;

public abstract interface SalesMultianswerRepository  extends JpaRepository<SalesMultianswer, String>{
	
	public abstract List<SalesMultianswer> findByOrgiAndAnsweridOrderBySortindexAsc(String orgi ,String answerid);
	
}

