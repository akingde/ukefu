package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventSip;

public interface StatusEventSipRepository extends JpaRepository<StatusEventSip, String> {
	public StatusEventSip findByIdAndOrgi(String id, String orgi);
}
