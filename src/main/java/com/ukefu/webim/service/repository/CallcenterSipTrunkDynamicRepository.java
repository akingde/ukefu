package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.CallcenterSipTrunkDynamic;

public interface CallcenterSipTrunkDynamicRepository extends JpaRepository<CallcenterSipTrunkDynamic, String> {
	
	public abstract List<CallcenterSipTrunkDynamic> findBySiptrunkidAndHostidAndOrgi(String siptrunkid ,String hostid , String orgi);
	
	public abstract List<CallcenterSipTrunkDynamic> findByIdAndOrgi(String id , String orgi);
	
	public abstract List<CallcenterSipTrunkDynamic> findAll(Specification<CallcenterSipTrunkDynamic> spec) ;
	
	public List<CallcenterSipTrunkDynamic> findByIdInAndOrgi(List<String> ids,String orgi) ;
	
	public abstract List<CallcenterSipTrunkDynamic> findBySiptrunkidAndAreaAndHostidAndOrgi(String siptrunkid, String area, String hostid, String orgi);
	
}
