package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.UpdateJobDetail;

public abstract interface UpdateJobDetailRepository extends JpaRepository<UpdateJobDetail, String> {
	
	public abstract UpdateJobDetail findByIdAndOrgi(String id, String orgi);
}
