package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventHangupSip;

public interface StatusEventHangupSipRepository extends JpaRepository<StatusEventHangupSip, String> {
	public StatusEventHangupSip findByIdAndOrgi(String id, String orgi);
}
