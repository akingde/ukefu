package com.ukefu.webim.service.es;

import java.util.List;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ukefu.webim.web.model.Favorites;

public interface FavoritesEsCommonRepository {
	
	public Page<Favorites> findByQuery(BoolQueryBuilder boolQueryBuilder , String q , Pageable page) ;
	
	public Page<Favorites> findByCreaterAndModel(String creater , String model , String q , Pageable page) ;
	
	public List<Favorites> findByOrderidAndOrgi(String orderId , String orgi) ;
	
	List<Favorites> findByOrderidAndOrgiAndCreater(String orderid, String orgi, String creater);
	
}
