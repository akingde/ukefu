package com.ukefu.webim.service.rpc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.corundumstudio.socketio.SocketIOClient;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;
import com.ukefu.util.client.NettyClients;
import com.ukefu.util.rpc.RPCDataBean;

public class CallCenterTopicListener implements MessageListener<Object>{
	@Override
    public void onMessage(Message<Object> message) {
		RPCDataBean rpcDataBean = (RPCDataBean) message.getMessageObject() ;
		if(rpcDataBean!=null && !StringUtils.isBlank(rpcDataBean.getId())) {
			List<SocketIOClient> clients = new ArrayList<SocketIOClient>();
			clients.addAll(NettyClients.getInstance().getCallCenterClients().getClients(rpcDataBean.getId())) ;
			if(clients!=null && clients.size() > 0) {
				for(SocketIOClient client : clients){
					client.sendEvent(rpcDataBean.getEvent(), rpcDataBean.getData());
				}
			}
		}
    }
}
