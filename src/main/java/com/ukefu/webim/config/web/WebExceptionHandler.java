package com.ukefu.webim.config.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class WebExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    protected ModelAndView handleExceptions(Exception ex, WebRequest request) {
        if ("org.apache.catalina.connector.ClientAbortException".equals(ex.getClass().getName())) {
            return null;
        }
        ex.printStackTrace();
        return new ModelAndView("/error") ;
    }

}