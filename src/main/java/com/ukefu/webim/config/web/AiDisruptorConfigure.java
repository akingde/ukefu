package com.ukefu.webim.config.web;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.ukefu.util.event.AiEvent;
import com.ukefu.webim.util.disruptor.ai.AiEventFactory;
import com.ukefu.webim.util.disruptor.ai.AiEventHandler;

@Component
public class AiDisruptorConfigure {
    
    @SuppressWarnings({ "unchecked", "deprecation" })
	@Bean(name="ai")   
    public Disruptor<AiEvent> ai() { 
    	Executor executor = Executors.newCachedThreadPool();
    	 AiEventFactory factory = new AiEventFactory();
    	 Disruptor<AiEvent> disruptor = new Disruptor<AiEvent>(factory, 1024, executor, ProducerType.SINGLE , new YieldingWaitStrategy());
    	 disruptor.setDefaultExceptionHandler(new UKeFuExceptionHandler());
    	 disruptor.handleEventsWith(new AiEventHandler());
    	 disruptor.start();
         return disruptor;   
    }  
}
