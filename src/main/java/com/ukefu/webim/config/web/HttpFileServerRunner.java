package com.ukefu.webim.config.web;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ukefu.webim.service.httpserver.HttpFileServer;
  
@Component  
public class HttpFileServerRunner implements CommandLineRunner { 
	
	@Value("${uk.file.server.port}")
	private String port;
	
	@Value("${uk.file.server.redirect}")
	private String redirect;
	
	@Value("${web.upload-path}")
    private String path;

	public HttpFileServer init() {  
		HttpFileServer server = null ;
		if(!StringUtils.isBlank(port) && port.matches("[\\d]{2,5}")) {
			server = new HttpFileServer(Integer.parseInt(port) , path , redirect) ;
			server.start();
		}
		return server ;
    }

    public void run(String... args) throws Exception { 
       this.init();
    }  
}  