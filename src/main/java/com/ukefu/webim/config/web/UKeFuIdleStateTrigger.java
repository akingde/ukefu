package com.ukefu.webim.config.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

public class UKeFuIdleStateTrigger extends ChannelInboundHandlerAdapter{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.channel().close();
	}
	
	@Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		if(evt instanceof IdleStateEvent) {
			IdleState state = ((IdleStateEvent)evt).state() ;
			if(state == IdleState.READER_IDLE ) {
				logger.info("KeepAlive Read Idle Tip Message: Time out (5000ms)");
				//客户端5S没有回复消息
				ctx.channel().close();
			}
		}else {
			super.userEventTriggered(ctx, evt);
		}
    }
}
