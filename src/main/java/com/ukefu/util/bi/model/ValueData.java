package com.ukefu.util.bi.model;

import java.text.DecimalFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.Interval;
import org.joda.time.Period;

import com.ukefu.util.UKTools;
import com.ukefu.webim.web.model.ColumnProperties;
import com.ukefu.webim.web.model.ColumnPropertiesStrReplace;

public class ValueData implements java.io.Serializable,Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4732877738059927547L;
	private String name ;
	private Object value ;
	private ValueData mergevalue ;
	private int rowspan =1;
	private int colspan =1;
	private boolean merge = false ;
	private String foramatValue ;
	private String formatStr ;
	private Level row = null;
	private Level col = null ;
	private String valueType ;
	private boolean canbedrill ;
	private String cellmergeid ;
	private String drillthroughsql ;
	private Object tempValue ;
	private String url ;			//钻取的时候保存的URL，在ReportDataFactor中设置
	private String target ;			//钻取的时候保存的target，在ReportDataFactor中设置
	private String vtclass ;		//指标数据类型 对应的  html style class
	private String valueStyle;		//预警一块的格式化样式，背景色，字体什么的
	
	private String style ;
	
	public ValueData(Object value , String foramatValue, String valueType , boolean canbedrill , String sql , String name , String formatStr , List<ColumnProperties> cols , int colindex){
		this.foramatValue = foramatValue ;
		this.value = value ;
		if(!StringUtils.isBlank(foramatValue) && foramatValue.toLowerCase().indexOf("nan") >= 0) {
			this.foramatValue = "" ;
			this.value = null ;
		}else if("time".equals(foramatValue) && this.value != null){
			this.foramatValue = UKTools.getDuration(0,(long)Float.parseFloat(String.valueOf(this.value))*1000) ;
		}
		this.valueType = valueType ;
		this.drillthroughsql = sql ;
		this.canbedrill = canbedrill ;
		this.name = name ;
		this.formatStr = formatStr ;
		if(cols!=null) {
			int index = 0 ;
			for(ColumnProperties col : cols) {
				if(!"order".equals(col.getType())) {
					if(index == colindex) {
						this.name = col.getTitle();
						if ("Infinity".equals(this.foramatValue)) {
							this.foramatValue = "0";
						}
						if(!StringUtils.isBlank(col.getPlaceholder()) && (StringUtils.isBlank(this.foramatValue) || this.foramatValue.contains("Infinity"))) {
							this.foramatValue = col.getPlaceholder() ;
							if(this.foramatValue.matches("^[-\\+]?[\\d]*$") && !StringUtils.isBlank(col.getFormat())) {
								this.foramatValue = new DecimalFormat(col.getFormat()).format(Double.parseDouble(col.getPlaceholder())) ;
							}
						}else if("0".equals(col.getFormat()) && !StringUtils.isBlank(this.foramatValue) && Long.parseLong(this.foramatValue)>=0) {
							this.foramatValue = UKTools.getDuration(0, Long.parseLong(this.foramatValue)) ;
						}else if("00".equals(col.getFormat()) && !StringUtils.isBlank(this.foramatValue) && Long.parseLong(this.foramatValue)>=0){
							Interval interval = new Interval(0 , Long.parseLong(this.foramatValue));
							Period p = interval.toPeriod();
							int hour = 0;
							if(p.getDays() > 0){
								hour = p.getDays()*24 + p.getHours();
							}else{
								hour = p.getHours();
							}
							int min = 0;
							if (hour>0) {
								min = hour*60;
							}
							String millis = String.valueOf(p.getMillis());
							if (p.getMillis()>100) {
								millis = millis.substring(0, 2);
							}else if(p.getMillis()<10){
								millis = "00";
							}
							this.foramatValue = new StringBuffer().append(UKTools.defaultNumberFormt.format(min+p.getMinutes())).append(":").append(UKTools.defaultNumberFormt.format(p.getSeconds())).append(".").append(millis).toString();
						}else if(col.isRequired()) {
							if(this.value!=null) {
								double number = Double.parseDouble(this.value.toString()) ;
								if(number < 0 || (foramatValue.indexOf("%")>0 && number > 1)) {
									if(!StringUtils.isBlank(col.getPlaceholder())) {
										this.foramatValue = col.getPlaceholder() ;
									}else {
										this.foramatValue = "" ;
									}
								}
							}
						}
						List<ColumnPropertiesStrReplace> cpsrList = col.getColumnPropertiesStrReplace();
						if (cpsrList!= null && cpsrList.size() > 0) {
							for(ColumnPropertiesStrReplace cpsr : cpsrList){
								if ((!StringUtils.isBlank(cpsr.getWord()) && cpsr.getWord().equals(this.foramatValue)) 
										|| (StringUtils.isBlank(cpsr.getWord()) && StringUtils.isBlank(this.foramatValue) )) {
									this.foramatValue = cpsr.getRepword() ;
								}
							}
						}
					}
					index++ ;
				}
			}
		}
	}
	
	public ValueData(String name , Object value, String foramatValue , String valueType){
		this.name = name;
		this.value = value ; 
		this.valueType = valueType ;
		this.foramatValue = foramatValue ;
	}
	
	public ValueData(String name , Object value, String foramatValue , String valueType , String coltype){
		this.name = name;
		this.value = value ; 
		this.valueType = valueType ;
		this.foramatValue = foramatValue ;
	}
	
	public Object getValue() {
		return this.mergevalue!=null && this.mergevalue!=this ? mergevalue.getValue() : value;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public String getForamatValue() {
		return foramatValue;
	}


	public void setForamatValue(String foramatValue) {
		this.foramatValue = foramatValue;
	}

	public String getValueType() {
		return valueType;
	}
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
	public String getDrillthroughsql() {
		return drillthroughsql;
	}
	public void setDrillthroughsql(String drillthroughsql) {
		this.drillthroughsql = drillthroughsql;
	}
	public String toString(){
		return this.foramatValue ;
	}

	public boolean isCanbedrill() {
		return canbedrill;
	}

	public void setCanbedrill(boolean canbedrill) {
		this.canbedrill = canbedrill;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Level getRow() {
		return row;
	}
	public void setRow(Level row) {
		this.row = row;
	}
	public Level getCol() {
		return col;
	}
	public void setCol(Level col) {
		this.col = col;
	}
	public Level getRowData(String title){
		Level level = this.row ;
		if(level!=null && level.getDimname()!=null){
			while(!level.getDimname().equals(title)){
				level = level.getParent();
				if(level!=null && level.getParent()==null){
					level = null ;
					break ;
				}
			}
		}
		return level;
	}
	public int getRowspan() {
		return rowspan == 0 ? 1 : rowspan;
	}
	public void setRowspan(int rowspan) {
		this.rowspan = rowspan;
	}
	public int getColspan() {
		return colspan == 0 ? 1 : colspan;
	}
	public void setColspan(int colspan) {
		this.colspan = colspan;
	}
	public boolean isMerge() {
		return merge;
	}
	public void setMerge(boolean merge) {
		this.merge = merge;
	}
	public String getCellmergeid() {
		return cellmergeid;
	}
	public void setCellmergeid(String cellmergeid) {
		this.cellmergeid = cellmergeid;
	}
	public String getFormatStr() {
		return formatStr;
	}
	public void setFormatStr(String formatStr) {
		this.formatStr = formatStr;
	}
	public Object getTempValue() {
		return tempValue;
	}
	public void setTempValue(Object tempValue) {
		this.tempValue = tempValue;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getVtclass() {
		return vtclass;
	}
	public void setVtclass(String vtclass) {
		this.vtclass = vtclass;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	/**
	 * 页面控制CSS使用 ，在 table.html
	 * 
	 * @return
	 */
	public String getDataid(){
		return UKTools.md5(this.name!=null ? this.name : "") ;
	}
	public String getValueStyle() {
		return valueStyle;
	}
	public void setValueStyle(String valueStyle) {
		this.valueStyle = valueStyle;
	}
	public ValueData getMergevalue() {
		return mergevalue;
	}
	public void setMergevalue(ValueData mergevalue) {
		this.mergevalue = mergevalue;
	}
	

	@Override  
    public ValueData clone() {  
        ValueData valueData = null;  
        try{  
        	valueData = (ValueData)super.clone();  
        }catch(CloneNotSupportedException e) {  
            e.printStackTrace();  
        }  
        return valueData;  
    }  
}
