package com.ukefu.util.task.process;

import com.ukefu.webim.service.repository.WorkserviceTimeRepository;
import com.ukefu.webim.web.model.WorkserviceTime;

public class WorktimeserviceProcess implements JPAProcess{
	
	private WorkserviceTimeRepository workserviceTimeRepository ;
	
	public WorktimeserviceProcess(WorkserviceTimeRepository workserviceTimeRepository){
		this.workserviceTimeRepository = workserviceTimeRepository ;
	}

	@Override
	public void process(Object data) {
		workserviceTimeRepository.save((WorkserviceTime)data) ;
	}

	
	@Override
	public void end() {
		
	}

}
