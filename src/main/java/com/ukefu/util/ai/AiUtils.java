package com.ukefu.util.ai;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.repository.SceneItemRepository;
import com.ukefu.webim.web.model.AiConfig;
import com.ukefu.webim.web.model.AiUser;
import com.ukefu.webim.web.model.SceneItem;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.UKeFuDic;

public class AiUtils {
	private static AiDicTrie aiDicTrie = new AiDicTrie();
	
	/**
	 * 初始化 AI语料库
	 * @param orgi
	 * @throws IOException
	 * @throws JcsegException 
	 */
	public static AiDicTrie init(String orgi) throws IOException{
		aiDicTrie.clean();
		SceneItemRepository sceneItemRes = UKDataContext.getContext().getBean(SceneItemRepository.class) ;
		List<SceneItem> sceneItemList = sceneItemRes.findByOrgiAndItemtype(orgi, UKDataContext.AiItemType.USERINPUT.toString()) ;
		for(SceneItem item : sceneItemList){
			if(!StringUtils.isBlank(item.getInputcon())) {
				for(String type : item.getInputcon().split(",")) {
					String[] types =  new String[] {type} ;
					for(String tp : types) {
						aiDicTrie.insertDic(tp , item.getSceneid());
					}
				}
			}else {
				aiDicTrie.insert(item.getContent(), item.getSceneid());
			}
		}
		return aiDicTrie;
	}
	
	public static AiDicTrie getAiDicTrie(){
		return aiDicTrie ;
	}
	
	
	/**
	 * AI配置
	 * @param orgi
	 * @return
	 */
	public static AiConfig initAiConfig(String aiid,String orgi){
		return UKTools.initAiConfig(aiid, orgi) ;
	}
	
	/**
	 * 机器人会话数据补全
	 * @param msg
	 * @param busslist
	 * @param bussop
	 * @param aiUser
	 */
	public static SysDic processAiUserNames(String msg , String busslist , boolean bussop , AiUser aiUser ,String bussclist , String regexs) {
		SysDic datadic = null;
		if(bussop && !StringUtils.isBlank(busslist)) {
			String[] codes =  busslist.split(",") ;
			List<SysDic> dicList = UKeFuDic.getInstance().getDic("com.dic.xiaoe.input") ;
			for(SysDic dic : dicList) {
				for(String code : codes) {
					if(dic.getCode().equals(code) || dic.getId().equals(code) ) {
						datadic = dic ;
						if(!StringUtils.isBlank(dic.getIconstr()) && (dic.getIconstr().equals("switch") || dic.getIconstr().equals("checkbox"))) {
							aiUser.getNames().put(dic.getCode() ,"1") ;
						}else if(!StringUtils.isBlank(dic.getIconstr()) && ( 
								dic.getIconstr().equals("radio")  || dic.getIconstr().equals("select"))) {
							aiUser.getNames().put(dic.getCode() , bussclist) ;
						}else {
							String value = null ;
							
							if(!StringUtils.isBlank(regexs)) {
								String[] regexarr = regexs.split("[,，]");
								for(String regex : regexarr) {
									if(!StringUtils.isBlank(regex) && msg.matches(regex.replaceAll(" ",""))) {
										Pattern pattern = Pattern.compile(regex.replaceAll(" ","")); 
										Matcher matcher = pattern.matcher(msg) ;
										if(matcher.find() && matcher.groupCount() >= 1) {
											value = matcher.group(1) ;
											 
										}
										break ;
									}
								}
							}else {
								value = msg ;
							}
							
							//Value为空，表示没有从配置的答案里获取到 内容，或者答案的内容里没哟分组
							if(!StringUtils.isBlank(dic.getDescription())) {
								if(dic.getDescription().equals("city") && aiUser.getIpdata()!=null && aiUser.getIpdata().getCity()!=null) {
									
									if(StringUtils.isBlank(value)) {
										value = aiUser.getIpdata().getCity() ;
										aiUser.getNames().put(dic.getCode()+".fixed" , value) ;
									}else if(dic.getDescription().equals("city") && aiUser.getIpdata().getCity().indexOf(value) == -1){
										aiUser.getNames().put(dic.getCode()+".fixed" , value) ;
										
									}
								}
							}
							if(!StringUtils.isBlank(value)) {
								if(dic.isDiscode() && aiUser.getNames().get(code) != null) {
									value = aiUser.getNames().get(code) +","+ value ;
								}
								if(value.length() > 255) {
									value = value.substring(0, 255) ;
								}
								aiUser.getNames().put(dic.getCode() , value) ;
							}
							
							
						}
						break ;
					}
				}
			}
		}
		return datadic;
	}
}
